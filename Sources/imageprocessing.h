//
//  imageprocessing.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-25.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @file
 *
 * Various image processing functions.
 */

/**
 * Convenience macro which expands to the offset of the pixel at (x, y) in 8-bit grayscale
 * image with the specified height and stride.
 *
 * @param x       The X coordinate of the pixel.
 * @param y       The Y coordinate of the pixel.
 * @param height  The height of the image.
 * @param stride  The number of bytes per row in the image.
 */
 #define GRAYPIXEL(x, y, height, stride) (((height) - (y) - 1) * (stride) + (x))

@class GenericImage;

/**
 * Returns the specified transform concatenated to itself n - 1 times, essentially
 * transform^n. Specifying a negative for n uses the inverse of the transform instead.
 *
 * @param transform  The transform.
 * @param n          The "power".
 *
 * @return The transform raised to the power of n.
 */
CGAffineTransform multiTransform(CGAffineTransform transform, int n);

/**
 * Returns an 8-bit RGBA \c CGBitmapContext with the given width and height.
 *
 * @param width   The width.
 * @param height  The height.
 *
 * @return An RGBA bitmap context.
 */
CGContextRef createRgbaBitmap(int width, int height);

/**
 * Returns an 8-bit grayscale \c CGBitmapContext with the given with and height.
 *
 * @param width  The width.
 * @param height  The height.
 *
 * @return A grayscale bitmap context.
 */
CGContextRef createGrayscaleBitmap(int width, int height);

/**
 * Creates a grayscale version of the specified image.
 *
 * @param image  The image to convert to grayscale.
 *
 * @return A grayscale version of the image.
 */
CGImageRef createGrayscale(CGImageRef image);

/**
 * Returns the sum of all pixels in \c context that are inside \c rect.
 * This function only operates on 8-bit grayscale bitmap contexts.
 *
 * @param context  The bitmap.
 * @param rect     The rectangle to consider.
 *
 * @return The pixel sum.
 */
int sumPixels(CGContextRef context, CGRect rect);

/**
 * Returns the sum all squared pixels in \c context that are inside \c rect.
 * This function only operates on 8-bit grayscale bitmap contexts.
 *
 * @param context  The context.
 * @param rect     The rectangle to consider.
 * @return The sum of squared pixels.
 */
float sumSquaredPixels(CGContextRef context, CGRect rect);

/**
 * Returns a \c CGBitmapContext that contains the difference between every corresponding
 * pixel in the specified images that are inside \c rect. This is done by drawing one image
 * on top of the other using the difference blending mode.
 *
 * @param image1  An image.
 * @param image2  Another image.
 * @param rect    The rectangle to consider.
 *
 * @return A bitmap of the pixel differences.
 */
CGContextRef createDiffmap(GenericImage *image1, GenericImage *image2, CGRect rect);

/**
 * Compares the two images by calculating the sum of squared differences between each
 * corresponding pixel in \c rect. A lower value means higher similarity. Pixels are
 * interpreted as a value between 0.0 and 1.0.
 *
 * @param image1  An image.
 * @param image2  Another image.
 * @param rect    The rectangle to consider.
 *
 * @return The sum of squared differences.
 */
float compareImages(GenericImage *image1, GenericImage *image2, CGRect rect);

/**
 * Returns the maximum distance to any edge of \c rect from the point \c p.
 *
 * @param p     The point.
 * @param rect  The rectangle.
 *
 * @return The maximum edge distance.
 */
CGFloat maxEdgeDistance(CGPoint p, CGRect rect);

/**
 * Returns a singleton device dependant RGB colorspace.
 *
 * @return An RGB colorspace.
 */
CGColorSpaceRef rgbColorspace();

/**
 * Returns a singleton device dependant gray colorspace.
 *
 * @return A gray colorspace.
 */
CGColorSpaceRef grayColorspace();

/**
 * Returns the sum of squared differences between each corresponding pixel in the two bitmaps inside the rectangle
 * specified by \c compRect. \c bitmap is offset by (offsetX, offsetY) in this comparison.
 *
 * This function only operates on 8-bit grayscale bitmap contexts.
 *
 * @param reference  The reference bitmap.
 * @param bitmap     The offset bitmap.
 * @param offsetX    The X axis offset.
 * @param offsetY    The Y axis offset.
 * @param compRect   The rectangle to consider.
 *
 * @return The sum of squared differences of the pixels inside compRect where each pixel is interpreted
 *         as a value between 0.0 and 1.0.
 */
float fastCompareBitmaps(CGContextRef reference, CGContextRef bitmap, int offsetX, int offsetY, CGRect compRect);

/**
 * Aligns \c image with \c ref by running a sort of "hill climbing" algorithm using the specified transforms.
 * The algorithm works by looking at each neighboring transform within the search radius and selecting the best
 * value. This is then repeated until there is no improvement in image similarity.
 *
 * A "neighboring transform" in this case is calculated as transform1^n1 * transform^n2 where n1 and n2 are chosen
 * in the range of -radius1 to radius1 and -radius2 to radius2 respectively.
 *
 * @param ref         The reference image.
 * @param image       The image to align.
 * @param transform1  The first transform.
 * @param transform2  The second transform.
 * @param radius1     The search radius of the first tranform.
 * @param radius2     The search radius of the second transform.
 * @param compRect    The rectangle in which to perform the comparison for each transform.
 */
void hillClimbAlign2D(GenericImage *ref,
					  GenericImage *image,
					  CGAffineTransform transform1,
					  CGAffineTransform transform2,
					  int radius1,
					  int radius2,
					  CGRect compRect);

/**
 * Aligns \c image with \c ref by using a simple 2D position alignment. This function is faster than using
 * \c hillClimbAlign2D since non-subpixel position alignment requires no drawing or transforming of the image
 * and thus comparisons can be done much faster. Otherwise, the algorithm is pretty much the same.
 *
 * @param ref       The reference image.
 * @param image     The image to align.
 * @param radiusX   The X axis search radius.
 * @param radiusY   The Y axis search radius.
 * @param compRect  The rectangle in which to perform the comparison for each offset.
 */
void fastPositionAlign(GenericImage *ref,
					   GenericImage *image,
					   int radiusX,
					   int radiusY,
					   CGRect compRect);

/**
 * Aligns \c image with \c reference by calling \c fastPositionAlign for different scaled versions of the images.
 * This function works by starting with downscaled versions of the images at the scale 1.0 / 2^startPower and then working
 * down to full scale, each time using fastPositionAlign with an X and Y radius of 1. Even though the scale of the images
 * increases all the time, the size of the comparison rectangle is kept constant and is always centered around \c point.
 *
 * @param reference   The reference image.
 * @param image       The image to align.
 * @param area        The comparison area of the images. The comparison rect for the smallest scale will be equal to this
 *                    and for each larger scale, the comparison rect will be placed inside this area of the image.
 * @param point       The center point of the comparison rect in each pass. The comparison rect cannot always be placed
 *                    centered around this point if that would place it outside of \c area.
 * @param startPoint  The starting power used to calculate the downscaling.
 */
void multiPassPositionAlign(GenericImage *reference, GenericImage *image, CGRect area, CGPoint point, int startPower);

/**
 * Returns the transform which transforms the points in \c f to the points in \c t. Since this function takes only
 * two from-points and two to-points, this function assumes that scaling and rotation is uniform (without skewing).
 *
 * @param f  A pointer to the from-points.
 * @param t  A pointer to the to-points.
 *
 * @return The transform as described above.
 */
CGAffineTransform registrationTransform2p(const CGPoint *f, const CGPoint *t);

/**
 * Returns the transform which transforms the three points in \c f to the three points in \c t.
 *
 * @param f  A point to the from-points.
 * @param t  A point to the to-points.
 *
 * @return The transform as described above.
 */
CGAffineTransform registrationTransform3p(const CGPoint *f, const CGPoint *t);

/**
 * Creates a map of the cornerness of each pixel in \c bitmap using a variant of the Trajkovic corner detector
 * with a "square radius" instead of a circular one. The result is an integer map placed in \c dest and the dimensions will
 * be equal to the dimensions of \c bitmap. The total size required is width * height.
 *
 * @param bitmap  The bitmap context to build the cornerness map from.
 * @param radius  The radius used for detection.
 * @param dest    A pointer to the output data.
 */
void createCornernessMap(CGContextRef bitmap, int radius, int *dest);

/**
 * Returns the maximum point in the given rectangle of the given integer bitmap.
 *
 * @param bitmap  The integer bitmap to search.
 * @param width   The width of the bitmap.
 * @param height  The height of the bitmap.
 * @param rect    The rectangle to search.
 * @param score   On return if non-NULL, contains the value of the max point which was returned.
 *
 * @return The max point as a \c CGPoint.
 */
CGPoint findMaxPoint(const int *bitmap, int width, int height, CGRect rect, int *score);

/**
 * Splits the given integer bitmap into grid (with the dimensions divider * divider) and selects the
 * max point from each cell in this grid. The grid is limited to \c area.
 *
 * @param bitmap   The integer bitmap to select the points from.
 * @param width    The width of the bitmap.
 * @param height   The height of the bitmap.
 * @param area     The area to limit the grid to.
 * @param divider  The number of grid divisions on both of the axes.
 *
 * @return An \c NSDictionary mapping the selected points to their values in the bitmap.
 */
NSDictionary *selectMaxPoints(const int *bitmap, int width, int height, CGRect area, int divider);

/**
 * Returns the point which would yield the best "score" together. The score of two points is computed
 * as min(score1, score2) * sqrt(distance) where \c score1 and \c score2 are the individual scores of the points
 * and \c distance is the distance between them. This means that the points that are close to \c point are avoided.
 *
 * @param point          The first point.
 * @param pointScore     The score of the first point.
 * @param points         An \c NSDictionary which maps all of the points to their respective score.
 * @param combinedScore  On return if non-NULL, contains the combined score of the two points as described above
 *
 * @return The select point.
 */
CGPoint selectSecondPoint(CGPoint point, int pointScore, NSDictionary *points, int *combinedScore);

/**
 * Selects the two points from \c points that have greatest combined score as described in the documentation for
 * \c selectSecondPoint.
 *
 * @param points          An \c NSDictionary which maps all of the points to their respective score.
 * @param selectedPoints  On return, contains the two selected points.
 */
void selectPoints(NSDictionary *points, CGPoint *selectedPoints);

/**
 * Finds corner points in \c image using \c createCornernessMap and \c selectMaxPoints. This will yield
 * divider * divider points inside \c area.
 *
 * @param image    The image.
 * @param area     The area to search.
 * @param divider  The divider as described in the documentation for \c selectMaxPoints.
 *
 * @return An \c NSDictionary which maps the selected points to their respective scores.
 */
NSDictionary *findCornerPoints(GenericImage *image, CGRect area, int divider);

/**
 * If \c rect is not completely contained inside \c constraint, this method returns a rect
 * that is moved the minimum amount to completely contain it in \c constraint. Otherwise, this method
 * simply returns \c rect.
 *
 * @param rect        The rectangle to constrain.
 * @param constraint  The constraint rectangle.
 *
 * @return The constrained rectangle.
 */
CGRect constrainToRect(CGRect rect, CGRect constraint);