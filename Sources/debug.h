//
//  debug.h
//  SmoothCam
//
//  Created by Emil Eriksson on 12/31/10.
//  Copyright 2010 N/A. All rights reserved.
//

/**
 * @file
 *
 * Miscellaneous debugging functions.
 */

#import <Foundation/Foundation.h>

@class GenericImage;

void saveImage(CGImageRef image, NSString *filename);
void saveBitmap(CGContextRef context, NSString *filename);
void saveDebugDiffmap(GenericImage *image1, GenericImage *image2, CGRect rect, NSString *filename);
void saveDebugCornerness(GenericImage *image, int radius, NSString *filename);
void saveImageWithPoints(GenericImage *image, NSArray *points, CGFloat size, NSString *filename);