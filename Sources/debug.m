//
//  debug.m
//  SmoothCam
//
//  Created by Emil Eriksson on 12/31/10.
//  Copyright 2010 N/A. All rights reserved.
//

#import "debug.h"

#import <Foundation/Foundation.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "GenericImage.h"
#import "imageprocessing.h"

void saveImage(CGImageRef image, NSString *filename) {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *path = [NSString stringWithFormat:@"%@/%@.tiff", [paths objectAtIndex:0], filename];
	NSURL *destFile = [NSURL fileURLWithPath:path];
	CGImageDestinationRef imageDest = CGImageDestinationCreateWithURL((CFURLRef)destFile, kUTTypeTIFF, 1, NULL);
	CGImageDestinationAddImage(imageDest, image, NULL);
	CGImageDestinationFinalize(imageDest);
	CFRelease(imageDest);
} 

void saveBitmap(CGContextRef context, NSString *filename) {
	CGImageRef image = CGBitmapContextCreateImage(context);
	saveImage(image, filename);
	CFRelease(image);
}

void saveDebugDiffmap(GenericImage *image1, GenericImage *image2, CGRect rect, NSString *filename) {
	CGContextRef context = createDiffmap(image1, image2, rect);
	saveBitmap(context, filename);
	CFRelease(context);
}

void saveImageWithPoints(GenericImage *image, NSArray *points, CGFloat size, NSString *filename) {
	CGContextRef context = [image createBitmap:image.bounds];
	CGContextSetGrayStrokeColor(context, 1.0f, 1.0f);
	CGContextSetBlendMode(context, kCGBlendModeDifference);
	CGContextSetLineWidth(context, size / 10.0f);
	for(NSValue *value in points) {
		CGPoint p = [value CGPointValue];
		CGContextStrokeEllipseInRect(context, CGRectMake(p.x - (size / 2.0f), p.y - (size / 2.0f), size, size));
	}
	saveBitmap(context, filename);
	CFRelease(context);
}