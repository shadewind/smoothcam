//
//  settings.h
//  SmoothCam
//
//  Created by Emil Eriksson on 01/01/11.
//  Copyright 2010 N/A. All rights reserved.
//

/**
 * @file
 *
 * Defines various keys and values for \c NSUserDefaults.
 */

/// The number for photos to take for a merge shot.
static NSString * const kNumMergePhotosKey   = @"numMergePhotos";
/// The action to take in case of single tap on the camera view.
static NSString * const kSingleTapActionKey  = @"singleTapAction";
/// The tap focus mode key.
static NSString * const kTapFocusModeKey     = @"tapFocusMode";
/// The output file format key.
static NSString * const kOutputFileFormatKey = @"outputFormat";
/// The key used to store the asset URLs for the gallery.
static NSString * const kGalleryUrlsKey      = @"galleryUrls";
/// Stores whether to save a reference image for comparison when doing a merge shoot.
static NSString * const kSaveMergeReference  = @"saveMergeReference";

/// Defines the values for \c kSingleTapActionKey.
typedef enum {
	/// Single tap sets focus and exposure.
	kSingleTapActionFocusExposure = 0,
	/// Single tap sets focus only.
	kSingleTapActionFocusOnly     = 1
} SingleTapAction;

/// Defines the values for \c kTapFocusModeKey.
typedef enum {
	/// Tap focus sets focus locked.
	kTapFocusModeLocked = 0,
	/// Tap focus sets focus mode continuous.
	kTapFocusModeContinuous = 1
} TapFocusMode;

/// Defines the values for \c kOutputFileFormatKey.
typedef enum {
	/// JPEG
	kOutputFileFormatJPEG = 0,
	/// TIFF
	kOutputFileFormatTIFF = 1
} OutputFileFormat;