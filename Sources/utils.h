/*
 *  utils.h
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-08.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

/**
 * @file
 *
 * Various utility functions.
 */

/**
 * Returns the rectangle with the same aspect ratio as \c r1 but maximized and centered in
 * \c r2.
 *
 * @param r1  The rectangle to fit.
 * @param r2  The rectangle to fit into.
 *
 * @return The fitted rectangle.
 */
CGRect fitRectInRect(CGRect r1, CGRect r2);

/**
 * Returns the specified integer value clamped to the range of
 * \c low - \c high.
 *
 * @param low    The minimum value.
 * @param value  The value.
 * @param high   The maximum value.
 *
 * @return The clamped value.
 */
static inline int clamp(int low, int value, int high) {
	return (value < low) ? low : ((value > high) ? high : value);
}

/**
 * Returns the specified integer value clamped to the range of
 * \c low - \c high.
 *
 * @param low    The minimum value.
 * @param value  The value.
 * @param high   The maximum value.
 *
 * @return The clamped value.
 */
static inline float clampf(float low, float value, float high) {
	return (value < low) ? low : ((value > high) ? high : value);
}