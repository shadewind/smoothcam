//
//  main.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-13.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
