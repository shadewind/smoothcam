/*
 *  utils.m
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-08.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

#import "utils.h"

CGRect fitRectInRect(CGRect r1, CGRect r2) {
	CGFloat a1 = r1.size.width / r1.size.height;
	CGFloat a2 = r2.size.width / r2.size.height;
	
	CGRect result;
	if(a1 < a2) {
		result.size.height = r2.size.height;
		result.size.width = result.size.height * a1;		
		result.origin.x = CGRectGetMidX(r2) - (result.size.width / 2);
		result.origin.y = r2.origin.y;
	} else {
		result.size.width = r2.size.width;
		result.size.height = result.size.width / a1;
		result.origin.x = r2.origin.x;
		result.origin.y = CGRectGetMidY(r2) - (result.size.height / 2);
	}
	
	return result;
}