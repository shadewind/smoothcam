//
//  uiutils.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-25.
//  Copyright 2010 N/A. All rights reserved.
//

#import "uiutils.h"

void drawRoundedRect(CGContextRef ctx, CGRect rect, CGFloat radius, CGPathDrawingMode drawingMode) {
	CGFloat minX = CGRectGetMinX(rect);
	CGFloat maxX = CGRectGetMaxX(rect);
	CGFloat minY = CGRectGetMinY(rect);
	CGFloat maxY = CGRectGetMaxY(rect);
	CGContextBeginPath(ctx);
	
	CGContextMoveToPoint(ctx, minX, minY + radius);
	CGContextAddLineToPoint(ctx, minX, maxY - radius);
	CGContextAddArcToPoint(ctx, minX, maxY, minX + radius, maxY, radius);
	CGContextAddLineToPoint(ctx, maxX - radius, maxY);
	CGContextAddArcToPoint(ctx, maxX, maxY, maxX, maxY - radius, radius);
	CGContextAddLineToPoint(ctx, maxX, minY + radius);
	CGContextAddArcToPoint(ctx, maxX, minY, maxX - radius, minY, radius);
	CGContextAddLineToPoint(ctx, minX + radius, minY);
	CGContextAddArcToPoint(ctx, minX, minY, minX, minY + radius, radius);
	CGContextClosePath(ctx);
	CGContextDrawPath(ctx, drawingMode);
}

float interfaceOrientationToAngle(UIInterfaceOrientation orientation) {
	switch(orientation) {
		case UIInterfaceOrientationLandscapeLeft:
			return -M_PI / 2;
		case UIInterfaceOrientationLandscapeRight:
			return M_PI / 2;
		case UIInterfaceOrientationPortraitUpsideDown:
			return M_PI;
		default:
			return 0.0f;
	}
}

EXIFOrientation deviceOrientationToExifOrienation(UIDeviceOrientation orientation) {
	switch(orientation) {
		case UIDeviceOrientationLandscapeLeft:
			return EXIFOrientationUp;
		case UIDeviceOrientationLandscapeRight:
			return EXIFOrientationDown;
		case UIDeviceOrientationPortraitUpsideDown:
			return EXIFOrientationRight;
		default:
			return EXIFOrientationLeft;
	}
}

UIImageOrientation exifOrientationToUIImageOrientation(EXIFOrientation exifOrientation) {
	switch(exifOrientation) {
		case EXIFOrientationUp:
			return UIImageOrientationUp;
		case EXIFOrientationRight:
			return UIImageOrientationLeft;
		case EXIFOrientationDown:
			return UIImageOrientationDown;
		case EXIFOrientationLeft:
			return UIImageOrientationRight;
		case EXIFOrientationUpMirrored:
			return UIImageOrientationUpMirrored;
		case EXIFOrientationRightMirrored:
			return UIImageOrientationLeftMirrored;
		case EXIFOrientationDownMirrored:
			return UIImageOrientationDownMirrored;
		case EXIFOrientationLeftMirrored:
			return UIImageOrientationRightMirrored;
		default:
			return UIImageOrientationUp;
	}
}