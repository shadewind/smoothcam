/*
 *  tupleset.h
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-12.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

#import <CoreFoundation/CoreFoundation.h>

/**
 * @file
 *
 * Defines functions to create \c CFMutableSet instances which store
 * \c Tuple structs.
 */

/**
 * Simple tuple struct which contains two values of arbitrary meaning.
 */
typedef struct {
	int v1;
	int v2;
} Tuple;

/**
 * Creates an empty \c CFMutableSet instance with functions set up for hashing, comparing
 * and releasing \c Tuple instances.
 */
CFMutableSetRef createTupleSet();

/**
 * Adds the specified tuple to the specified set.
 *
 * @param set    The set to add to.
 * @param tuple  The tuple to add.
 */
void addToTupleSet(CFMutableSetRef set, Tuple tuple);

/**
 * Returns a tuple with the specified values.
 *
 * @param v1  The value of \c v1.
 * @param v2  The value of \c v2.
 *
 * @return A tuple with the given values.
 */
Tuple tuple(int v1, int v2);

/**
 * Returns true if the specified tuples are equal, false if not.
 *
 * @return true or false depending on whether the tuples are equal or not.
 */
Boolean tuplesAreEqual(const void *value1, const void *value2);

/**
 * Returns the hash code for the specified tuple.
 *
 * @return The computed hash code.
 */
CFHashCode tupleHash(const void *value);

/**
 * Frees the specified tuple.
 *
 * @param allocator  Ignored.
 * @param value      The tuple to free.
 */
void deleteTuple(CFAllocatorRef allocator, const void *value);
