//
//  imageprocessing.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-25.
//  Copyright 2010 N/A. All rights reserved.
//

#import "imageprocessing.h"

#import <math.h>

#import "GenericImage.h"
#import "tupleset.h"
#import "debug.h"

#define TO_FLOAT_SQUARED(v) (((v) / 255.0f) * ((v) / 255.0f))
static const float byteToSquareFloatTable[] = {
	TO_FLOAT_SQUARED(255), TO_FLOAT_SQUARED(254), TO_FLOAT_SQUARED(253), TO_FLOAT_SQUARED(252), 
	TO_FLOAT_SQUARED(251), TO_FLOAT_SQUARED(250), TO_FLOAT_SQUARED(249), TO_FLOAT_SQUARED(248), 
	TO_FLOAT_SQUARED(247), TO_FLOAT_SQUARED(246), TO_FLOAT_SQUARED(245), TO_FLOAT_SQUARED(244), 
	TO_FLOAT_SQUARED(243), TO_FLOAT_SQUARED(242), TO_FLOAT_SQUARED(241), TO_FLOAT_SQUARED(240), 
	TO_FLOAT_SQUARED(239), TO_FLOAT_SQUARED(238), TO_FLOAT_SQUARED(237), TO_FLOAT_SQUARED(236), 
	TO_FLOAT_SQUARED(235), TO_FLOAT_SQUARED(234), TO_FLOAT_SQUARED(233), TO_FLOAT_SQUARED(232), 
	TO_FLOAT_SQUARED(231), TO_FLOAT_SQUARED(230), TO_FLOAT_SQUARED(229), TO_FLOAT_SQUARED(228), 
	TO_FLOAT_SQUARED(227), TO_FLOAT_SQUARED(226), TO_FLOAT_SQUARED(225), TO_FLOAT_SQUARED(224), 
	TO_FLOAT_SQUARED(223), TO_FLOAT_SQUARED(222), TO_FLOAT_SQUARED(221), TO_FLOAT_SQUARED(220), 
	TO_FLOAT_SQUARED(219), TO_FLOAT_SQUARED(218), TO_FLOAT_SQUARED(217), TO_FLOAT_SQUARED(216), 
	TO_FLOAT_SQUARED(215), TO_FLOAT_SQUARED(214), TO_FLOAT_SQUARED(213), TO_FLOAT_SQUARED(212), 
	TO_FLOAT_SQUARED(211), TO_FLOAT_SQUARED(210), TO_FLOAT_SQUARED(209), TO_FLOAT_SQUARED(208), 
	TO_FLOAT_SQUARED(207), TO_FLOAT_SQUARED(206), TO_FLOAT_SQUARED(205), TO_FLOAT_SQUARED(204), 
	TO_FLOAT_SQUARED(203), TO_FLOAT_SQUARED(202), TO_FLOAT_SQUARED(201), TO_FLOAT_SQUARED(200), 
	TO_FLOAT_SQUARED(199), TO_FLOAT_SQUARED(198), TO_FLOAT_SQUARED(197), TO_FLOAT_SQUARED(196), 
	TO_FLOAT_SQUARED(195), TO_FLOAT_SQUARED(194), TO_FLOAT_SQUARED(193), TO_FLOAT_SQUARED(192), 
	TO_FLOAT_SQUARED(191), TO_FLOAT_SQUARED(190), TO_FLOAT_SQUARED(189), TO_FLOAT_SQUARED(188), 
	TO_FLOAT_SQUARED(187), TO_FLOAT_SQUARED(186), TO_FLOAT_SQUARED(185), TO_FLOAT_SQUARED(184), 
	TO_FLOAT_SQUARED(183), TO_FLOAT_SQUARED(182), TO_FLOAT_SQUARED(181), TO_FLOAT_SQUARED(180), 
	TO_FLOAT_SQUARED(179), TO_FLOAT_SQUARED(178), TO_FLOAT_SQUARED(177), TO_FLOAT_SQUARED(176), 
	TO_FLOAT_SQUARED(175), TO_FLOAT_SQUARED(174), TO_FLOAT_SQUARED(173), TO_FLOAT_SQUARED(172), 
	TO_FLOAT_SQUARED(171), TO_FLOAT_SQUARED(170), TO_FLOAT_SQUARED(169), TO_FLOAT_SQUARED(168), 
	TO_FLOAT_SQUARED(167), TO_FLOAT_SQUARED(166), TO_FLOAT_SQUARED(165), TO_FLOAT_SQUARED(164), 
	TO_FLOAT_SQUARED(163), TO_FLOAT_SQUARED(162), TO_FLOAT_SQUARED(161), TO_FLOAT_SQUARED(160), 
	TO_FLOAT_SQUARED(159), TO_FLOAT_SQUARED(158), TO_FLOAT_SQUARED(157), TO_FLOAT_SQUARED(156), 
	TO_FLOAT_SQUARED(155), TO_FLOAT_SQUARED(154), TO_FLOAT_SQUARED(153), TO_FLOAT_SQUARED(152), 
	TO_FLOAT_SQUARED(151), TO_FLOAT_SQUARED(150), TO_FLOAT_SQUARED(149), TO_FLOAT_SQUARED(148), 
	TO_FLOAT_SQUARED(147), TO_FLOAT_SQUARED(146), TO_FLOAT_SQUARED(145), TO_FLOAT_SQUARED(144), 
	TO_FLOAT_SQUARED(143), TO_FLOAT_SQUARED(142), TO_FLOAT_SQUARED(141), TO_FLOAT_SQUARED(140), 
	TO_FLOAT_SQUARED(139), TO_FLOAT_SQUARED(138), TO_FLOAT_SQUARED(137), TO_FLOAT_SQUARED(136), 
	TO_FLOAT_SQUARED(135), TO_FLOAT_SQUARED(134), TO_FLOAT_SQUARED(133), TO_FLOAT_SQUARED(132), 
	TO_FLOAT_SQUARED(131), TO_FLOAT_SQUARED(130), TO_FLOAT_SQUARED(129), TO_FLOAT_SQUARED(128), 
	TO_FLOAT_SQUARED(127), TO_FLOAT_SQUARED(126), TO_FLOAT_SQUARED(125), TO_FLOAT_SQUARED(124), 
	TO_FLOAT_SQUARED(123), TO_FLOAT_SQUARED(122), TO_FLOAT_SQUARED(121), TO_FLOAT_SQUARED(120), 
	TO_FLOAT_SQUARED(119), TO_FLOAT_SQUARED(118), TO_FLOAT_SQUARED(117), TO_FLOAT_SQUARED(116), 
	TO_FLOAT_SQUARED(115), TO_FLOAT_SQUARED(114), TO_FLOAT_SQUARED(113), TO_FLOAT_SQUARED(112), 
	TO_FLOAT_SQUARED(111), TO_FLOAT_SQUARED(110), TO_FLOAT_SQUARED(109), TO_FLOAT_SQUARED(108), 
	TO_FLOAT_SQUARED(107), TO_FLOAT_SQUARED(106), TO_FLOAT_SQUARED(105), TO_FLOAT_SQUARED(104), 
	TO_FLOAT_SQUARED(103), TO_FLOAT_SQUARED(102), TO_FLOAT_SQUARED(101), TO_FLOAT_SQUARED(100), 
	TO_FLOAT_SQUARED(99),  TO_FLOAT_SQUARED(98),  TO_FLOAT_SQUARED(97),  TO_FLOAT_SQUARED(96), 
	TO_FLOAT_SQUARED(95),  TO_FLOAT_SQUARED(94),  TO_FLOAT_SQUARED(93),  TO_FLOAT_SQUARED(92), 
	TO_FLOAT_SQUARED(91),  TO_FLOAT_SQUARED(90),  TO_FLOAT_SQUARED(89),  TO_FLOAT_SQUARED(88), 
	TO_FLOAT_SQUARED(87),  TO_FLOAT_SQUARED(86),  TO_FLOAT_SQUARED(85),  TO_FLOAT_SQUARED(84), 
	TO_FLOAT_SQUARED(83),  TO_FLOAT_SQUARED(82),  TO_FLOAT_SQUARED(81),  TO_FLOAT_SQUARED(80), 
	TO_FLOAT_SQUARED(79),  TO_FLOAT_SQUARED(78),  TO_FLOAT_SQUARED(77),  TO_FLOAT_SQUARED(76), 
	TO_FLOAT_SQUARED(75),  TO_FLOAT_SQUARED(74),  TO_FLOAT_SQUARED(73),  TO_FLOAT_SQUARED(72), 
	TO_FLOAT_SQUARED(71),  TO_FLOAT_SQUARED(70),  TO_FLOAT_SQUARED(69),  TO_FLOAT_SQUARED(68), 
	TO_FLOAT_SQUARED(67),  TO_FLOAT_SQUARED(66),  TO_FLOAT_SQUARED(65),  TO_FLOAT_SQUARED(64), 
	TO_FLOAT_SQUARED(63),  TO_FLOAT_SQUARED(62),  TO_FLOAT_SQUARED(61),  TO_FLOAT_SQUARED(60), 
	TO_FLOAT_SQUARED(59),  TO_FLOAT_SQUARED(58),  TO_FLOAT_SQUARED(57),  TO_FLOAT_SQUARED(56), 
	TO_FLOAT_SQUARED(55),  TO_FLOAT_SQUARED(54),  TO_FLOAT_SQUARED(53),  TO_FLOAT_SQUARED(52), 
	TO_FLOAT_SQUARED(51),  TO_FLOAT_SQUARED(50),  TO_FLOAT_SQUARED(49),  TO_FLOAT_SQUARED(48), 
	TO_FLOAT_SQUARED(47),  TO_FLOAT_SQUARED(46),  TO_FLOAT_SQUARED(45),  TO_FLOAT_SQUARED(44), 
	TO_FLOAT_SQUARED(43),  TO_FLOAT_SQUARED(42),  TO_FLOAT_SQUARED(41),  TO_FLOAT_SQUARED(40), 
	TO_FLOAT_SQUARED(39),  TO_FLOAT_SQUARED(38),  TO_FLOAT_SQUARED(37),  TO_FLOAT_SQUARED(36), 
	TO_FLOAT_SQUARED(35),  TO_FLOAT_SQUARED(34),  TO_FLOAT_SQUARED(33),  TO_FLOAT_SQUARED(32), 
	TO_FLOAT_SQUARED(31),  TO_FLOAT_SQUARED(30),  TO_FLOAT_SQUARED(29),  TO_FLOAT_SQUARED(28), 
	TO_FLOAT_SQUARED(27),  TO_FLOAT_SQUARED(26),  TO_FLOAT_SQUARED(25),  TO_FLOAT_SQUARED(24), 
	TO_FLOAT_SQUARED(23),  TO_FLOAT_SQUARED(22),  TO_FLOAT_SQUARED(21),  TO_FLOAT_SQUARED(20), 
	TO_FLOAT_SQUARED(19),  TO_FLOAT_SQUARED(18),  TO_FLOAT_SQUARED(17),  TO_FLOAT_SQUARED(16), 
	TO_FLOAT_SQUARED(15),  TO_FLOAT_SQUARED(14),  TO_FLOAT_SQUARED(13),  TO_FLOAT_SQUARED(12), 
	TO_FLOAT_SQUARED(11),  TO_FLOAT_SQUARED(10),  TO_FLOAT_SQUARED(9),   TO_FLOAT_SQUARED(8), 
	TO_FLOAT_SQUARED(7),   TO_FLOAT_SQUARED(6),   TO_FLOAT_SQUARED(5),   TO_FLOAT_SQUARED(4), 
	TO_FLOAT_SQUARED(3),   TO_FLOAT_SQUARED(2),   TO_FLOAT_SQUARED(1),   
	TO_FLOAT_SQUARED(0),   TO_FLOAT_SQUARED(1),   TO_FLOAT_SQUARED(2),   TO_FLOAT_SQUARED(3), 
	TO_FLOAT_SQUARED(4),   TO_FLOAT_SQUARED(5),   TO_FLOAT_SQUARED(6),   TO_FLOAT_SQUARED(7), 
	TO_FLOAT_SQUARED(8),   TO_FLOAT_SQUARED(9),   TO_FLOAT_SQUARED(10),  TO_FLOAT_SQUARED(11), 
	TO_FLOAT_SQUARED(12),  TO_FLOAT_SQUARED(13),  TO_FLOAT_SQUARED(14),  TO_FLOAT_SQUARED(15), 
	TO_FLOAT_SQUARED(16),  TO_FLOAT_SQUARED(17),  TO_FLOAT_SQUARED(18),  TO_FLOAT_SQUARED(19), 
	TO_FLOAT_SQUARED(20),  TO_FLOAT_SQUARED(21),  TO_FLOAT_SQUARED(22),  TO_FLOAT_SQUARED(23), 
	TO_FLOAT_SQUARED(24),  TO_FLOAT_SQUARED(25),  TO_FLOAT_SQUARED(26),  TO_FLOAT_SQUARED(27), 
	TO_FLOAT_SQUARED(28),  TO_FLOAT_SQUARED(29),  TO_FLOAT_SQUARED(30),  TO_FLOAT_SQUARED(31), 
	TO_FLOAT_SQUARED(32),  TO_FLOAT_SQUARED(33),  TO_FLOAT_SQUARED(34),  TO_FLOAT_SQUARED(35), 
	TO_FLOAT_SQUARED(36),  TO_FLOAT_SQUARED(37),  TO_FLOAT_SQUARED(38),  TO_FLOAT_SQUARED(39), 
	TO_FLOAT_SQUARED(40),  TO_FLOAT_SQUARED(41),  TO_FLOAT_SQUARED(42),  TO_FLOAT_SQUARED(43), 
	TO_FLOAT_SQUARED(44),  TO_FLOAT_SQUARED(45),  TO_FLOAT_SQUARED(46),  TO_FLOAT_SQUARED(47), 
	TO_FLOAT_SQUARED(48),  TO_FLOAT_SQUARED(49),  TO_FLOAT_SQUARED(50),  TO_FLOAT_SQUARED(51), 
	TO_FLOAT_SQUARED(52),  TO_FLOAT_SQUARED(53),  TO_FLOAT_SQUARED(54),  TO_FLOAT_SQUARED(55), 
	TO_FLOAT_SQUARED(56),  TO_FLOAT_SQUARED(57),  TO_FLOAT_SQUARED(58),  TO_FLOAT_SQUARED(59), 
	TO_FLOAT_SQUARED(60),  TO_FLOAT_SQUARED(61),  TO_FLOAT_SQUARED(62),  TO_FLOAT_SQUARED(63), 
	TO_FLOAT_SQUARED(64),  TO_FLOAT_SQUARED(65),  TO_FLOAT_SQUARED(66),  TO_FLOAT_SQUARED(67), 
	TO_FLOAT_SQUARED(68),  TO_FLOAT_SQUARED(69),  TO_FLOAT_SQUARED(70),  TO_FLOAT_SQUARED(71), 
	TO_FLOAT_SQUARED(72),  TO_FLOAT_SQUARED(73),  TO_FLOAT_SQUARED(74),  TO_FLOAT_SQUARED(75), 
	TO_FLOAT_SQUARED(76),  TO_FLOAT_SQUARED(77),  TO_FLOAT_SQUARED(78),  TO_FLOAT_SQUARED(79), 
	TO_FLOAT_SQUARED(80),  TO_FLOAT_SQUARED(81),  TO_FLOAT_SQUARED(82),  TO_FLOAT_SQUARED(83), 
	TO_FLOAT_SQUARED(84),  TO_FLOAT_SQUARED(85),  TO_FLOAT_SQUARED(86),  TO_FLOAT_SQUARED(87), 
	TO_FLOAT_SQUARED(88),  TO_FLOAT_SQUARED(89),  TO_FLOAT_SQUARED(90),  TO_FLOAT_SQUARED(91), 
	TO_FLOAT_SQUARED(92),  TO_FLOAT_SQUARED(93),  TO_FLOAT_SQUARED(94),  TO_FLOAT_SQUARED(95), 
	TO_FLOAT_SQUARED(96),  TO_FLOAT_SQUARED(97),  TO_FLOAT_SQUARED(98),  TO_FLOAT_SQUARED(99), 
	TO_FLOAT_SQUARED(100), TO_FLOAT_SQUARED(101), TO_FLOAT_SQUARED(102), TO_FLOAT_SQUARED(103), 
	TO_FLOAT_SQUARED(104), TO_FLOAT_SQUARED(105), TO_FLOAT_SQUARED(106), TO_FLOAT_SQUARED(107), 
	TO_FLOAT_SQUARED(108), TO_FLOAT_SQUARED(109), TO_FLOAT_SQUARED(110), TO_FLOAT_SQUARED(111), 
	TO_FLOAT_SQUARED(112), TO_FLOAT_SQUARED(113), TO_FLOAT_SQUARED(114), TO_FLOAT_SQUARED(115), 
	TO_FLOAT_SQUARED(116), TO_FLOAT_SQUARED(117), TO_FLOAT_SQUARED(118), TO_FLOAT_SQUARED(119), 
	TO_FLOAT_SQUARED(120), TO_FLOAT_SQUARED(121), TO_FLOAT_SQUARED(122), TO_FLOAT_SQUARED(123), 
	TO_FLOAT_SQUARED(124), TO_FLOAT_SQUARED(125), TO_FLOAT_SQUARED(126), TO_FLOAT_SQUARED(127), 
	TO_FLOAT_SQUARED(128), TO_FLOAT_SQUARED(129), TO_FLOAT_SQUARED(130), TO_FLOAT_SQUARED(131), 
	TO_FLOAT_SQUARED(132), TO_FLOAT_SQUARED(133), TO_FLOAT_SQUARED(134), TO_FLOAT_SQUARED(135), 
	TO_FLOAT_SQUARED(136), TO_FLOAT_SQUARED(137), TO_FLOAT_SQUARED(138), TO_FLOAT_SQUARED(139), 
	TO_FLOAT_SQUARED(140), TO_FLOAT_SQUARED(141), TO_FLOAT_SQUARED(142), TO_FLOAT_SQUARED(143), 
	TO_FLOAT_SQUARED(144), TO_FLOAT_SQUARED(145), TO_FLOAT_SQUARED(146), TO_FLOAT_SQUARED(147), 
	TO_FLOAT_SQUARED(148), TO_FLOAT_SQUARED(149), TO_FLOAT_SQUARED(150), TO_FLOAT_SQUARED(151), 
	TO_FLOAT_SQUARED(152), TO_FLOAT_SQUARED(153), TO_FLOAT_SQUARED(154), TO_FLOAT_SQUARED(155), 
	TO_FLOAT_SQUARED(156), TO_FLOAT_SQUARED(157), TO_FLOAT_SQUARED(158), TO_FLOAT_SQUARED(159), 
	TO_FLOAT_SQUARED(160), TO_FLOAT_SQUARED(161), TO_FLOAT_SQUARED(162), TO_FLOAT_SQUARED(163), 
	TO_FLOAT_SQUARED(164), TO_FLOAT_SQUARED(165), TO_FLOAT_SQUARED(166), TO_FLOAT_SQUARED(167), 
	TO_FLOAT_SQUARED(168), TO_FLOAT_SQUARED(169), TO_FLOAT_SQUARED(170), TO_FLOAT_SQUARED(171), 
	TO_FLOAT_SQUARED(172), TO_FLOAT_SQUARED(173), TO_FLOAT_SQUARED(174), TO_FLOAT_SQUARED(175), 
	TO_FLOAT_SQUARED(176), TO_FLOAT_SQUARED(177), TO_FLOAT_SQUARED(178), TO_FLOAT_SQUARED(179), 
	TO_FLOAT_SQUARED(180), TO_FLOAT_SQUARED(181), TO_FLOAT_SQUARED(182), TO_FLOAT_SQUARED(183), 
	TO_FLOAT_SQUARED(184), TO_FLOAT_SQUARED(185), TO_FLOAT_SQUARED(186), TO_FLOAT_SQUARED(187), 
	TO_FLOAT_SQUARED(188), TO_FLOAT_SQUARED(189), TO_FLOAT_SQUARED(190), TO_FLOAT_SQUARED(191), 
	TO_FLOAT_SQUARED(192), TO_FLOAT_SQUARED(193), TO_FLOAT_SQUARED(194), TO_FLOAT_SQUARED(195), 
	TO_FLOAT_SQUARED(196), TO_FLOAT_SQUARED(197), TO_FLOAT_SQUARED(198), TO_FLOAT_SQUARED(199), 
	TO_FLOAT_SQUARED(200), TO_FLOAT_SQUARED(201), TO_FLOAT_SQUARED(202), TO_FLOAT_SQUARED(203), 
	TO_FLOAT_SQUARED(204), TO_FLOAT_SQUARED(205), TO_FLOAT_SQUARED(206), TO_FLOAT_SQUARED(207), 
	TO_FLOAT_SQUARED(208), TO_FLOAT_SQUARED(209), TO_FLOAT_SQUARED(210), TO_FLOAT_SQUARED(211), 
	TO_FLOAT_SQUARED(212), TO_FLOAT_SQUARED(213), TO_FLOAT_SQUARED(214), TO_FLOAT_SQUARED(215), 
	TO_FLOAT_SQUARED(216), TO_FLOAT_SQUARED(217), TO_FLOAT_SQUARED(218), TO_FLOAT_SQUARED(219), 
	TO_FLOAT_SQUARED(220), TO_FLOAT_SQUARED(221), TO_FLOAT_SQUARED(222), TO_FLOAT_SQUARED(223), 
	TO_FLOAT_SQUARED(224), TO_FLOAT_SQUARED(225), TO_FLOAT_SQUARED(226), TO_FLOAT_SQUARED(227), 
	TO_FLOAT_SQUARED(228), TO_FLOAT_SQUARED(229), TO_FLOAT_SQUARED(230), TO_FLOAT_SQUARED(231), 
	TO_FLOAT_SQUARED(232), TO_FLOAT_SQUARED(233), TO_FLOAT_SQUARED(234), TO_FLOAT_SQUARED(235), 
	TO_FLOAT_SQUARED(236), TO_FLOAT_SQUARED(237), TO_FLOAT_SQUARED(238), TO_FLOAT_SQUARED(239), 
	TO_FLOAT_SQUARED(240), TO_FLOAT_SQUARED(241), TO_FLOAT_SQUARED(242), TO_FLOAT_SQUARED(243), 
	TO_FLOAT_SQUARED(244), TO_FLOAT_SQUARED(245), TO_FLOAT_SQUARED(246), TO_FLOAT_SQUARED(247), 
	TO_FLOAT_SQUARED(248), TO_FLOAT_SQUARED(249), TO_FLOAT_SQUARED(250), TO_FLOAT_SQUARED(251), 
	TO_FLOAT_SQUARED(252), TO_FLOAT_SQUARED(253), TO_FLOAT_SQUARED(254), TO_FLOAT_SQUARED(255)
};

CGAffineTransform multiTransform(CGAffineTransform transform, int n) {
	if(n < 0)
		transform = CGAffineTransformInvert(transform);
	
	CGAffineTransform t = CGAffineTransformMakeTranslation(0, 0);
	for(int i = 0; i < abs((float)n); i++)
		t = CGAffineTransformConcat(t, transform);
	
	return t;
}

CGContextRef createRgbaBitmap(int width, int height) {
	int stride = width * 4;
	if((stride % 2) != 0)
		stride++;
	
	return CGBitmapContextCreate(NULL, width, height, 8, stride, rgbColorspace(),
								 kCGBitmapByteOrderDefault | kCGImageAlphaNoneSkipLast);
}

CGContextRef createGrayscaleBitmap(int width, int height) {
	int stride = width;
	if((stride % 2) != 0)
		stride++;
	
	return CGBitmapContextCreate(NULL, width, height, 8, stride, grayColorspace(), kCGBitmapByteOrderDefault);
}

CGImageRef createGrayscale(CGImageRef image) {
	int width = CGImageGetWidth(image);
	int height = CGImageGetHeight(image);
	CGContextRef context = createGrayscaleBitmap(width, height);
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
	CGImageRef grayscale = CGBitmapContextCreateImage(context);
	CFRelease(context);
	return grayscale;
}

int sumPixels(CGContextRef context, CGRect rect) {
	unsigned char *data = (unsigned char *)CGBitmapContextGetData(context);
	int width = CGBitmapContextGetWidth(context);
	int height = CGBitmapContextGetHeight(context);
	int stride = CGBitmapContextGetBytesPerRow(context);
	int startX = clamp(0, CGRectGetMinX(rect), width);
	int endX = clamp(0, CGRectGetMaxX(rect), width);
	int startY = clamp(0, CGRectGetMinY(rect), height);
	int endY = clamp(0, CGRectGetMaxY(rect), height);
	
	int sum = 0;
	for(int y = startY; y < endY; y++) {
		for(int x = startX; x < endX; x++) {
			sum += data[GRAYPIXEL(x, y, height, stride)];
		}
	}
	
	return sum;
}

float sumSquaredPixels(CGContextRef context, CGRect rect) {
	unsigned char *data = (unsigned char *)CGBitmapContextGetData(context);
	int width = CGBitmapContextGetWidth(context);
	int height = CGBitmapContextGetHeight(context);
	int stride = CGBitmapContextGetBytesPerRow(context);
	int startX = clamp(0, CGRectGetMinX(rect), width);
	int endX = clamp(0, CGRectGetMaxX(rect), width);
	int startY = clamp(0, CGRectGetMinY(rect), height);
	int endY = clamp(0, CGRectGetMaxY(rect), height);
	
	float sum = 0;
	for(int y = startY; y < endY; y++) {
		for(int x = startX; x < endX; x++) {
			int value = data[GRAYPIXEL(x, y, height, stride)];
			sum += byteToSquareFloatTable[value + 255];
		}
	}
	
	return sum;
}

CGContextRef createDiffmap(GenericImage *image1, GenericImage *image2, CGRect rect) {
	CGContextRef context = createGrayscaleBitmap(rect.size.width, rect.size.height);
	CGContextSetInterpolationQuality(context, kCGInterpolationLow);
	
	CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
	[image1 drawTo:context];
	CGContextSetBlendMode(context, kCGBlendModeDifference);
	[image2 drawTo:context];
	
	return context;
	
}

float compareImages(GenericImage *image1, GenericImage *image2, CGRect rect) {
	CGContextRef context = createDiffmap(image1, image2, rect);
	float sum = sumSquaredPixels(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
	CFRelease(context);
	return sum;
}

CGFloat maxEdgeDistance(CGPoint p, CGRect rect) {
	CGFloat minX = CGRectGetMinX(rect);
	CGFloat maxX = CGRectGetMaxX(rect);
	CGFloat minY = CGRectGetMinY(rect);
	CGFloat maxY = CGRectGetMaxY(rect);
	
	CGFloat dist;
	dist = fmax(hypot(minX - p.x, minY - p.y), hypot(maxX - p.x, maxY - p.y));
	dist = fmax(hypot(minX - p.x, maxY - p.y), dist);
	return fmax(hypot(maxX - p.x, minY - p.y), dist);
}

CGColorSpaceRef rgbColorspace() {
	static CGColorSpaceRef colorspace = NULL;
	if(colorspace == NULL)
		colorspace = CGColorSpaceCreateDeviceRGB();
	
	return colorspace;
}

CGColorSpaceRef grayColorspace() {
	static CGColorSpaceRef colorspace = NULL;
	if(colorspace == NULL)
		colorspace = CGColorSpaceCreateDeviceGray();
	
	return colorspace;
}

float fastCompareBitmaps(CGContextRef reference, CGContextRef bitmap, int offsetX, int offsetY, CGRect compRect) {
	unsigned char *refData = CGBitmapContextGetData(reference);
	int refStride = CGBitmapContextGetBytesPerRow(reference);
	int refWidth = CGBitmapContextGetWidth(reference);
	int refHeight = CGBitmapContextGetHeight(reference);
	unsigned char *data = CGBitmapContextGetData(bitmap);
	int stride = CGBitmapContextGetBytesPerRow(bitmap);
	int width = CGBitmapContextGetWidth(bitmap);
	int height = CGBitmapContextGetHeight(bitmap);
	
	int minX = clamp(offsetX, CGRectGetMinX(compRect), fmin(refWidth, width + offsetX));
	int maxX = clamp(offsetX, CGRectGetMaxX(compRect), fmin(refWidth, width + offsetX));
	int minY = clamp(offsetY, CGRectGetMinY(compRect), fmin(refHeight, height + offsetY));
	int maxY = clamp(offsetY, CGRectGetMaxY(compRect), fmin(refHeight, height + offsetY));
	
	float sum = 0;
	for(int y = minY; y < maxY; y++) {
		for(int x = minX; x < maxX; x++) {
			int v1 = refData[GRAYPIXEL(x, y, refHeight, refStride)];
			int v2 = data[GRAYPIXEL(x - offsetX, y - offsetY, height, stride)];
			sum += byteToSquareFloatTable[v1 - v2 + 255];
		}
	}
	
	return sum;
}

void hillClimbAlign2D(GenericImage *ref,
					  GenericImage *image,
					  CGAffineTransform transform1,
					  CGAffineTransform transform2,
					  int radius1,
					  int radius2,
					  CGRect compRect)
{
	CFMutableSetRef checked = createTupleSet();
	Tuple origin;
	Tuple best;
	best.v1 = 0;
	best.v2 = 0;
	float bestValue = INFINITY;
	int count = 0;
	do {
		count++;
		origin = best;
		for(int i = -radius1; i <= radius1; i++) {
			for(int j = -radius2; j <= radius2; j++) {
				Tuple current = tuple(origin.v1 + i, origin.v2 + j);
				if(!CFSetContainsValue(checked, &current)) {
					addToTupleSet(checked, current);
					[image saveTransform];
					[image transform:multiTransform(transform1, current.v1)];
					[image transform:multiTransform(transform2, current.v2)];
					float value = compareImages(ref, image, compRect);
					[image restoreTransform];
					if(value < bestValue) {
						bestValue = value;
						best = current;
					}
				}
			}
		}
	} while(!tuplesAreEqual(&origin, &best));
	
	[image transform:multiTransform(transform1, best.v1)];
	[image transform:multiTransform(transform2, best.v2)];
	
	CFRelease(checked);
}

void fastPositionAlign(GenericImage *ref,
					   GenericImage *image,
					   int radiusX,
					   int radiusY,
					   CGRect compRect)
{
	CGRect bitmapRect;
	bitmapRect.size.width = compRect.size.width * 1.50f;
	bitmapRect.size.height = compRect.size.height * 1.50f;
	bitmapRect.origin.x = CGRectGetMidX(compRect) - (bitmapRect.size.width / 2.0f);
	bitmapRect.origin.y = CGRectGetMidY(compRect) - (bitmapRect.size.height / 2.0f);
	CGRect modCompRect = compRect;
	modCompRect.origin.x -= bitmapRect.origin.x;
	modCompRect.origin.y -= bitmapRect.origin.y;
	
	CGContextRef refBitmap = [ref createBitmap:bitmapRect];
	CGContextRef imageBitmap = [image createBitmap:bitmapRect];
	
	CFMutableSetRef checked = createTupleSet();
	Tuple origin;
	Tuple best;
	best.v1 = 0;
	best.v2 = 0;
	float bestValue = INFINITY;
	do {
		origin = best;
		for(int x = -radiusX; x <= radiusX; x++) {
			for(int y = -radiusY; y <= radiusY; y++) {
				Tuple current = tuple(origin.v1 + x, origin.v2 + y);
				if(!CFSetContainsValue(checked, &current)) {
					addToTupleSet(checked, current);
					float value = fastCompareBitmaps(refBitmap, imageBitmap, current.v1, current.v2, modCompRect);
					if(value < bestValue) {
						bestValue = value;
						best = current;
					}
				}
			}
		}
	} while(!tuplesAreEqual(&origin, &best));
	
	CFRelease(checked);
	CFRelease(refBitmap);
	CFRelease(imageBitmap);
	
	[image transform:CGAffineTransformMakeTranslation(best.v1, best.v2)];
}

void multiPassPositionAlign(GenericImage *reference, GenericImage *image, CGRect area, CGPoint point, int startPower) {
	CGFloat startScale = 1.0f / exp2(startPower);
	CGRect compRect = CGRectApplyAffineTransform(area, CGAffineTransformMakeScale(startScale, startScale));
	for(int i = startPower; i >= 0; i--) {
		CGFloat scale = 1.0f / exp2(i);
		CGAffineTransform scaleTransform = CGAffineTransformMakeScale(scale, scale);
		CGRect scaledArea = CGRectApplyAffineTransform(area, scaleTransform);
		CGPoint scaledPoint = CGPointApplyAffineTransform(point, scaleTransform);
		compRect.origin.x = scaledPoint.x - (CGRectGetWidth(compRect) / 2.0f);
		compRect.origin.y = scaledPoint.y - (CGRectGetHeight(compRect) / 2.0f);
		compRect = constrainToRect(compRect, scaledArea);
		
		[reference saveTransform];
		[reference transform:scaleTransform];
		[image transform:scaleTransform];
		fastPositionAlign(reference, image, 1, 1, compRect);
		[image transform:CGAffineTransformInvert(scaleTransform)];
		[reference restoreTransform];
	}
}

CGAffineTransform registrationTransform2p(const CGPoint *f, const CGPoint *t) {
	CGAffineTransform tf;
	
	CGFloat a = -((f[0].y - f[1].y) * (t[0].y - t[1].y) - (f[1].x - f[0].x) * (t[0].x - t[1].x)) /
	             ((f[0].y - f[1].y) * (f[1].y - f[0].y) - (f[1].x - f[0].x) * (f[1].x - f[0].x));
	
	CGFloat b = -(f[0].y*t[0].x - f[1].y*t[0].x - f[0].x*t[0].y + f[1].x*t[0].y - f[0].y*t[1].x + f[1].y*t[1].x + f[0].x*t[1].y - f[1].x*t[1].y) / 
	             (f[0].x*f[0].x + f[0].y*f[0].y - 2*f[0].x*f[1].x + f[1].x*f[1].x - 2*f[0].y*f[1].y + f[1].y*f[1].y);

	tf.a = a;
	tf.b = b;
	tf.c = -b;
	tf.d = a;
	
	tf.tx = -(f[0].x*f[1].x*t[0].x - f[1].x*f[1].x*t[0].x + f[0].y*f[1].y*t[0].x - f[1].y*f[1].y*t[0].x +
			  f[0].y*f[1].x*t[0].y - f[0].x*f[1].y*t[0].y - f[0].x*f[0].x*t[1].x - f[0].y*f[0].y*t[1].x +
			  f[0].x*f[1].x*t[1].x + f[0].y*f[1].y*t[1].x - f[0].y*f[1].x*t[1].y + f[0].x*f[1].y*t[1].y) /
	         (f[0].x*f[0].x + f[0].y*f[0].y - 2*f[0].x*f[1].x + f[1].x*f[1].x - 2*f[0].y*f[1].y + f[1].y*f[1].y);
	
	tf.ty = -(f[0].x*f[1].y*t[0].x - f[0].y*f[1].x*t[0].x + f[0].x*f[1].x*t[0].y - f[1].x*f[1].x*t[0].y + f[0].y*f[1].y*t[0].y - f[1].y*f[1].y*t[0].y +
			  f[0].y*f[1].x*t[1].x - f[0].x*f[1].y*t[1].x - f[0].x*f[0].x*t[1].y - f[0].y*f[0].y*t[1].y + f[0].x*f[1].x*t[1].y + f[0].y*f[1].y*t[1].y) /
	         (f[0].x*f[0].x + f[0].y*f[0].y - 2*f[0].x*f[1].x + f[1].x*f[1].x - 2*f[0].y*f[1].y + f[1].y*f[1].y);
	
	return tf;
}

CGAffineTransform registrationTransform3p(const CGPoint *f, const CGPoint *t) {
	CGAffineTransform tf;
	
	tf.a = -(f[1].y*t[0].x - f[2].y*t[0].x - f[0].y*t[1].x + f[2].y*t[1].x + f[0].y*t[2].x - f[1].y*t[2].x) /
		    (f[1].x*f[0].y - f[2].x*f[0].y - f[0].x*f[1].y + f[2].x*f[1].y + f[0].x*f[2].y - f[1].x*f[2].y);
	
	tf.b = -(f[1].y*t[0].y - f[2].y*t[0].y - f[0].y*t[1].y + f[2].y*t[1].y + f[0].y*t[2].y - f[1].y*t[2].y) /
		    (f[1].x*f[0].y - f[2].x*f[0].y - f[0].x*f[1].y + f[2].x*f[1].y + f[0].x*f[2].y - f[1].x*f[2].y);
	
	tf.c = -( f[1].x*t[0].x - f[2].x*t[0].x - f[0].x*t[1].x + f[2].x*t[1].x + f[0].x*t[2].x - f[1].x*t[2].x) /
		    (-f[1].x*f[0].y + f[2].x*f[0].y + f[0].x*f[1].y - f[2].x*f[1].y - f[0].x*f[2].y + f[1].x*f[2].y);
	
	tf.d = -(-f[1].x*t[0].y + f[2].x*t[0].y + f[0].x*t[1].y - f[2].x*t[1].y - f[0].x*t[2].y + f[1].x*t[2].y) /
		    ( f[1].x*f[0].y - f[2].x*f[0].y - f[0].x*f[1].y + f[2].x*f[1].y + f[0].x*f[2].y - f[1].x*f[2].y);
	
	tf.tx = -( f[2].x*f[1].y*t[0].x - f[1].x*f[2].y*t[0].x - f[2].x*f[0].y*t[1].x + f[0].x*f[2].y*t[1].x + f[1].x*f[0].y*t[2].x - f[0].x*f[1].y*t[2].x) /
			 (-f[1].x*f[0].y + f[2].x*f[0].y + f[0].x*f[1].y - f[2].x*f[1].y - f[0].x*f[2].y + f[1].x*f[2].y);
	
	tf.ty = -(-f[2].x*f[1].y*t[0].y + f[1].x*f[2].y*t[0].y + f[2].x*f[0].y*t[1].y - f[0].x*f[2].y*t[1].y - f[1].x*f[0].y*t[2].y + f[0].x*f[1].y*t[2].y) /
			 ( f[1].x*f[0].y - f[2].x*f[0].y - f[0].x*f[1].y + f[2].x*f[1].y + f[0].x*f[2].y - f[1].x*f[2].y);
	
	return tf;
}

void createCornernessMap(CGContextRef bitmap, int radius, int *dest) {
	int width = CGBitmapContextGetWidth(bitmap);
	int height = CGBitmapContextGetHeight(bitmap);
	int stride = CGBitmapContextGetBytesPerRow(bitmap);
	unsigned char *srcData = CGBitmapContextGetData(bitmap);
	
	for(int y = radius; y < (height - radius); y++) {
		for(int x = radius; x < (width - radius); x++) {
			int pc = srcData[GRAYPIXEL(x, y, height, stride)];
			int minValue = INT_MAX;
			
			for(int i = -radius; i <= radius; i++) {
				int pt = srcData[GRAYPIXEL(x + i, y + radius, height, stride)];
				int pb = srcData[GRAYPIXEL(x - i, y - radius, height, stride)];
				int pl = srcData[GRAYPIXEL(x - radius, y + i, height, stride)];
				int pr = srcData[GRAYPIXEL(x + radius, y - i, height, stride)];
				
				int tb = (pc - pt)*(pc - pt) + (pc - pb)*(pc - pb);
				int lr = (pc - pl)*(pc - pl) + (pc - pr)*(pc - pr);
				int value = (tb < lr) ? tb : lr;
				if(value < minValue)
					minValue = value;
			}
			
			dest[width * (height - y - 1) + x] = minValue;
		}
	}
}

CGPoint findMaxPoint(const int *bitmap, int width, int height, CGRect rect, int *score) {
	int maxValue = 0;
	CGPoint bestPoint = CGPointZero;
	for(int y = CGRectGetMinY(rect); y < CGRectGetMaxY(rect); y++) {
		for(int x = CGRectGetMinX(rect); x < CGRectGetMaxX(rect); x++) {
			int value = bitmap[width * (height - y - 1) + x];
			if(value > maxValue) {
				maxValue = value;
				bestPoint = CGPointMake(x, y);
			}
		}
	}
	if(score != NULL)
		*score = maxValue;
	return bestPoint;
}

NSDictionary *selectMaxPoints(const int *bitmap, int width, int height, CGRect area, int divider) {
	CGFloat rectW = CGRectGetWidth(area) / divider;
	CGFloat rectH = CGRectGetHeight(area) / divider;
	NSMutableDictionary *points = [NSMutableDictionary dictionaryWithCapacity:divider * divider];
	for(int y = 0; y < divider; y++) {
		for(CGFloat x = 0; x < divider; x++) {
			CGRect rect = CGRectMake(area.origin.x + (x * rectW),
									 area.origin.y + (y * rectH),
									 rectW, rectH);
			int score;
			CGPoint point = findMaxPoint(bitmap, width, height, rect, &score);
			[points setObject:[NSNumber numberWithInt:score] forKey:[NSValue valueWithCGPoint:point]];
		}
	}
	
	return points;
}

CGPoint selectSecondPoint(CGPoint point, int pointScore, NSDictionary *points, int *combinedScore) {
	CGPoint bestPoint = CGPointZero;
	int bestScore = 0;
	for(NSValue *key in points) {
		CGPoint p = [key CGPointValue];
		int secondPointScore = [[points objectForKey:key] intValue];
		int lowestScore = (pointScore < secondPointScore) ? pointScore : secondPointScore;
		int score = lowestScore * sqrt(hypot(p.x - point.x, p.y - point.y));
		if(score > bestScore) {
			bestPoint = p;
			bestScore = score;
		}
	}
	
	if(combinedScore != NULL)
		*combinedScore = bestScore;
	return bestPoint;
}

void selectPoints(NSDictionary *points, CGPoint *selectedPoints) {
	int bestScore = 0;
	selectedPoints[0] = CGPointZero;
	selectedPoints[1] = CGPointZero;
	for(NSValue *key in points) {
		CGPoint p = [key CGPointValue];
		int pScore = [[points objectForKey:key] intValue];
		int combinedScore;
		CGPoint secondPoint = selectSecondPoint(p, pScore, points, &combinedScore);
		if(combinedScore > bestScore) {
			selectedPoints[0] = p;
			selectedPoints[1] = secondPoint;
			bestScore = combinedScore;
		}
	}
}

NSDictionary *findCornerPoints(GenericImage *image, CGRect area, int divider) {
	static const float scale = 0.25f;
	int bitmapWidth = image.width * scale;
	int bitmapHeight = image.height * scale;
	CGContextRef context = createGrayscaleBitmap(bitmapWidth, bitmapHeight);
	CGAffineTransform scaleTransform = CGAffineTransformMakeScale(scale, scale);
	CGContextConcatCTM(context, scaleTransform);
	[image drawTo:context];
	
	int *cornernessMap = (int *)malloc(sizeof(int) * bitmapWidth * bitmapHeight);
	createCornernessMap(context, 10, cornernessMap);
	NSDictionary *scaledPoints = selectMaxPoints(cornernessMap, bitmapWidth, bitmapHeight,
										   CGRectApplyAffineTransform(area, scaleTransform), divider);
	CFRelease(context);
	free(cornernessMap);
	
	NSMutableDictionary *points = [NSMutableDictionary dictionaryWithCapacity:[scaledPoints count]];
	CGAffineTransform inverseScale = CGAffineTransformInvert(scaleTransform);
	for(NSValue *key in scaledPoints) {
		CGPoint point = CGPointApplyAffineTransform([key CGPointValue], inverseScale);
		[points setObject:[scaledPoints objectForKey:key] forKey:[NSValue valueWithCGPoint:point]];
	}
	
	return points;
}

CGRect constrainToRect(CGRect rect, CGRect constraint) {
	if(CGRectGetMinX(rect) < CGRectGetMinX(constraint))
		rect.origin.x = constraint.origin.x;
	if(CGRectGetMaxX(rect) > CGRectGetMaxX(constraint))
		rect.origin.x = CGRectGetMaxX(constraint) - CGRectGetWidth(rect);
	if(CGRectGetMinY(rect) < CGRectGetMinY(constraint))
		rect.origin.y = constraint.origin.y;
	if(CGRectGetMaxY(rect) > CGRectGetMaxY(constraint))
		rect.origin.y = CGRectGetMaxY(constraint) - CGRectGetHeight(rect);
	
	return rect;
}