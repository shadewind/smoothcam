//
//  uiutils.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-25.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ImageMetadata.h"

/**
 * @file
 *
 * Various (more or less) UI related functions.
 */

/**
 * Draws a rounded rectangle.
 *
 * @param ctx          The \c CGContext to draw to.
 * @param rect         The rectangle specifying the frame of the rounded rectangle.
 * @param radius       The corner radius.
 * @param drawingMode  The drawing mode.
 */
void drawRoundedRect(CGContextRef ctx, CGRect rect, CGFloat radius, CGPathDrawingMode drawingMode);

/**
 * Returns the rotation angle for the for the specified interface orientation where
 * portrait orientation returns 0.0.
 *
 * @param orientation  The orientation.
 *
 * @return The interface rotation.
 */
CGFloat interfaceOrientationToAngle(UIInterfaceOrientation orientation);

/**
 * Returns the EXIF/TIFF Orientation value which corresponds to the specified
 * \c UIDeviceOrientation.
 *
 * @param orientation  The \c UIDeviceOrientation.
 *
 * @return The EXIF/TIFF Orientation.
 */
EXIFOrientation deviceOrientationToExifOrienation(UIDeviceOrientation orientation);

/**
 * Returns the \c UIDeviceOrientation which corresponds to the specified EXIF/TIFF Orientation.
 *
 * @param exitOrientation  The EXIF/TIFF orientation.
 *
 * @return The corresponding \c UIImageOrientation.
 */
UIImageOrientation exifOrientationToUIImageOrientation(EXIFOrientation exifOrientation);
