//
//  colorconversion.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-05.
//  Copyright 2011 N/A. All rights reserved.
//

/**
 * @file
 *
 * Functions for color conversion.
 */

/**
 * Converts the given Y'CbCr image data to RGB. The output RGB data is
 * 32-bit RGBA data with the alpha component last which is also left
 * untouched.
 *
 * @param width      The width of the image.
 * @param height     The height of the image.
 * @param yBase      A pointer to the base of the Y plane image data.
 * @param yStride    The number of bytes per row of the Y plane.
 * @param cbBase     A pointer to the base of the Cb plane image data.
 * @param cbStride   The number of bytes per row of the Cb plane.
 * @param crBase     A pointer to the base of the Cr plane image data.
 * @param crStride   The number of bytes per row for the Cr plane.
 * @param rgbBase    A point to the destination RGB data buffer.
 * @param rgbStride  The number of bytes per row for the output RGB data.
 */
void ycbrToRgb(int width, int height,
			   const unsigned char *yBase, int yStride,
			   const unsigned char *cbBase, int cbStride,
			   const unsigned char *crBase, int crStride,
			   unsigned char *rgbBase, int rgbStride);