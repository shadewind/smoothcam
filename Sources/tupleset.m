/*
 *  tupleset.m
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-12.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

#import "tupleset.h"
	
CFMutableSetRef createTupleSet() {
	CFSetCallBacks callbacks;
	callbacks.version = 0;
	callbacks.copyDescription = NULL;
	callbacks.retain = NULL;
	callbacks.release = &deleteTuple;
	callbacks.equal = &tuplesAreEqual;
	callbacks.hash = &tupleHash;
	
	return CFSetCreateMutable(NULL, 0, &callbacks);
}

void addToTupleSet(CFMutableSetRef set, Tuple tuple) {
	Tuple *newTuple = malloc(sizeof(Tuple));
	newTuple->v1 = tuple.v1;
	newTuple->v2 = tuple.v2;
	CFSetAddValue(set, newTuple);
}

Tuple tuple(int v1, int v2) {
	Tuple tuple;
	tuple.v1 = v1;
	tuple.v2 = v2;
	return tuple;
}

Boolean tuplesAreEqual(const void *value1, const void *value2) {
	const Tuple *t1 = (const Tuple *)value1;
	const Tuple *t2 = (const Tuple *)value2;
	return (t1->v1 == t2->v1) && (t1->v2 == t2->v2);
}

CFHashCode tupleHash(const void *value) {
	const Tuple *tuple = (const Tuple *)value;
	return tuple->v1 + (tuple->v2 * 7);
}

void deleteTuple(CFAllocatorRef allocator, const void *value) {
	free((void *)value);
}