//
//  ImageViewController.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-14.
//  Copyright 2011 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AssetsLibrary/AssetsLibrary.h>

@class ImageScrollView;

@interface ImageViewController : UIViewController <UIScrollViewDelegate> {
	ImageScrollView *scrollView;
	ALAssetsLibrary *library;
	NSArray *urls;
	int index;
}

@property (retain, nonatomic) NSArray *urls;
@property (nonatomic) int index;

@end
