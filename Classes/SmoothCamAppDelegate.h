//
//  SmoothCamAppDelegate.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-13.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SmoothCamViewController;

@interface SmoothCamAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	SmoothCamViewController *rootController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SmoothCamViewController *rootController;

@end

