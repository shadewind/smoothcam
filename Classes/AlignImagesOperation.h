//
//  AlignImagesOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-16.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MultiImageOperation.h"

@class GenericImage;

@interface AlignImagesOperation : MultiImageOperation {
	GenericImage *grayscaleRef;
	CGPoint alignPoint[2];
	CGRect area;
}

@end
