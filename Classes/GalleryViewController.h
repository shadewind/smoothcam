//
//  GalleryViewController.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-13.
//  Copyright 2011 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AssetsLibrary/AssetsLibrary.h>

#import "JobQueueDelegate.h"

@class JobQueue, ImageViewController;

@interface GalleryViewController : UIViewController <JobQueueDelegate, UIActionSheetDelegate> {
	UIScrollView *scrollView;
	UIView *galleryView;
	ImageViewController *imageViewController;
	
	NSMutableArray *urls;
	NSMutableArray *assetViews;
	NSMutableArray *jobViews;
	CGPoint insertPoint;
	ALAssetsLibrary *library;
	
	JobQueue *jobQueue;
}

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *galleryView;
@property (retain, nonatomic) JobQueue *jobQueue;

@end
