//
//  ImageScrollView.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import "ImageScrollView.h"

#import "TilingImageView.h"

@implementation ImageScrollView

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if(self) {
		self.showsVerticalScrollIndicator = NO;
		self.showsHorizontalScrollIndicator = NO;
		self.decelerationRate = UIScrollViewDecelerationRateFast;
		self.delegate = self;
		self.autoresizesSubviews = NO;
	}
	
	return self;
}

- (void)displayImage:(UIImage *)image lowRes:(UIImage *)lowresImage {
	[imageView removeFromSuperview];
	imageView = [[TilingImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
	[self addSubview:imageView];
	[imageView release];
	
	imageView.image = image;
	imageView.lowresImage = lowresImage;
	self.contentSize = imageView.frame.size;
	[self updateZoomSettings];
	self.zoomScale = self.minimumZoomScale;
}

- (void)updateZoomSettings {
	float xScale = CGRectGetWidth(self.bounds) / imageView.bounds.size.width;
	float yScale = CGRectGetHeight(self.bounds) / imageView.bounds.size.height;
	self.minimumZoomScale = MIN(xScale, yScale);
	self.maximumZoomScale = 1.0f / [UIScreen mainScreen].scale;
	if(self.minimumZoomScale > self.maximumZoomScale)
		self.minimumZoomScale = self.maximumZoomScale;
}

- (CGPoint)centerPoint {
	CGPoint boundsCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
	return [self convertPoint:boundsCenter toView:imageView];
}

- (void)zoomToPoint:(CGPoint)p scale:(float)scale {
	self.zoomScale = MAX(self.minimumZoomScale, MIN(scale, self.maximumZoomScale));
	CGPoint boundsCenter = [self convertPoint:p fromView:imageView];
	CGPoint offset = CGPointMake(boundsCenter.x - (self.bounds.size.width / 2.0f),
								 boundsCenter.y - (self.bounds.size.height / 2.0f));
	offset.x = MAX(0.0f, MIN(offset.x, self.contentSize.width - self.bounds.size.width));
	offset.y = MAX(0.0f, MIN(offset.y, self.contentSize.height - self.bounds.size.height));
	self.contentOffset = offset;
}

- (void)zoomInToPoint:(CGPoint)p {
	if(self.minimumZoomScale != self.maximumZoomScale) {
		CGPoint center = [self convertPoint:p toView:imageView];
		CGSize rectSize = self.bounds.size;
		CGRect rect = CGRectMake(center.x - (rectSize.width / 2.0f),
								 center.y - (rectSize.height / 2.0f),
								 rectSize.width, rectSize.height);
		[self zoomToRect:rect animated:YES];
	}
}

- (void)layoutSubviews {
	CGSize size = self.bounds.size;
	CGRect frame = imageView.frame;
	
	if(size.width > frame.size.width)
		frame.origin.x = (size.width - frame.size.width) / 2.0f;
	else
		frame.origin.x = 0.0f;
		
	if(size.height > frame.size.height)
		frame.origin.y = (size.height - frame.size.height) / 2.0f;
	else
		frame.origin.y = 0.0f;
	
	imageView.frame = frame;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return imageView;
}

@end
