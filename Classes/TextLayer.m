//
//  TextLayer.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-12.
//  Copyright 2011 N/A. All rights reserved.
//

#import "TextLayer.h"

@interface TextLayer ()
- (void)setup;
@end

@implementation TextLayer

@synthesize text, font, color;

- (id)init {
	self = [super init];
	if(self)
		[self setup];
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(self)
		[self setup];
	
	return self;
}

- (id)initWithLayer:(id)layer {
	self = [super initWithLayer:layer];
	if(self)
		[self setup];
	
	return self;
}

- (void)setup {
	self.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
	self.color = [UIColor whiteColor];
}

- (void)setText:(NSString *)t {
	[text release];
	text = t;
	[text retain];
	[self setNeedsDisplay];
}

- (void)setFont:(UIFont *)f {
	[font release];
	font = f;
	[font retain];
	[self setNeedsDisplay];
}

- (void)setColor:(UIColor *)c {
	[color release];
	color = c;
	[color retain];
	[self setNeedsDisplay];
}

- (void)drawInContext:(CGContextRef)ctx {
	if(text == nil)
		return;
	
	UIGraphicsPushContext(ctx);
	CGSize textSize = [text sizeWithFont:font];
	CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
	CGPoint p = CGPointMake(center.x - (textSize.width / 2), center.y - (textSize.height / 2));
	CGContextSetFillColorWithColor(ctx, color.CGColor);
	[text drawAtPoint:p withFont:font];
	UIGraphicsPopContext();
}

- (void)dealloc {
	self.text = nil;
	self.font = nil;
	self.color = nil;
	
	[super dealloc];
}

@end
