//
//  JobQueueDelegate.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-17.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Job;

@protocol JobQueueDelegate <NSObject>

@optional
- (void)jobAdded:(Job *)job;
- (void)jobStarted:(Job *)job;
- (void)jobFinished:(Job *)job;

@end
