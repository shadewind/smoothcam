//
//  CameraSession.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "CameraSession.h"

#import <unistd.h>

#import "YCbCrImage.h"
#import "GenericImage.h"
#import "utils.h"
#import "uiutils.h"

@interface CameraSession ()
- (void)shootNext;
- (void)finishShooting;
- (void)safeSetFocusMode:(AVCaptureFocusMode)mode;
- (void)safeSetExposureMode:(AVCaptureExposureMode)mode;
@end

@implementation CameraSession

@synthesize previewLayer, delegate, orientation;

- (id)init {
	self = [super init];
	if(self) {
		nextId = 0;
		shotPictures = [[NSMutableArray alloc] initWithCapacity:10];
		captureSession = [[AVCaptureSession alloc] init];
		captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
		
		captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
		input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
		[captureSession addInput:input];
		
		output = [[AVCaptureStillImageOutput alloc] init];
		[captureSession addOutput:output];
		
		for(AVCaptureConnection *c in output.connections) {
			for(AVCaptureInputPort *port in c.inputPorts) {
				if(port.mediaType == AVMediaTypeVideo)
					connection = c;
			}
		}
		
		previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
		
		[captureDevice addObserver:self
						forKeyPath:@"adjustingFocus"
						   options:NSKeyValueObservingOptionNew
						   context:NULL];
		
		[captureSession startRunning];
		self.outputFormat = kOutputFormatYCbCr;
		
		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	}
	
	return self;
}

- (void)shootPictures:(int)n {
	if(![self isShooting]) {
		totalPictures = n;
		nextPicture = 0;
		
		if(n > 1) {
			if([captureDevice lockForConfiguration:nil]) {
				prevFocusMode = captureDevice.focusMode;
				prevExposureMode = captureDevice.exposureMode;
				[self safeSetFocusMode:AVCaptureFocusModeLocked];
				[self safeSetExposureMode:AVCaptureExposureModeLocked];
				[captureDevice unlockForConfiguration];
			}
		}
		
		[self shootNext];
	}
}

- (OutputFormat)outputFormat {
	return outputFormat;
}

- (void)setOutputFormat:(OutputFormat)format {
	outputFormat = format;
	[captureSession beginConfiguration];
	if(format == kOutputFormatRGBA) {
		output.outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithInt:kCVPixelFormatType_32BGRA],
								 kCVPixelBufferPixelFormatTypeKey,
								 AVVideoCodecJPEG, AVVideoCodecKey, nil];
	} else {
		output.outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange],
								 kCVPixelBufferPixelFormatTypeKey,
								 AVVideoCodecJPEG, AVVideoCodecKey, nil];
	}
	[captureSession commitConfiguration];
}

- (void)shootNext {
	if(nextPicture < totalPictures) {
		[output captureStillImageAsynchronouslyFromConnection:connection completionHandler:
			^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
				NSString *strId = [[NSNumber numberWithInt:nextId++] stringValue];
				
				Image *image;
				if(outputFormat == kOutputFormatRGBA)
					image = [[GenericImage alloc] initWithSampleBuffer:imageDataSampleBuffer id:strId];
				else
					image = [[YCbCrImage alloc] initWithSampleBuffer:imageDataSampleBuffer id:strId];
					
				image.metadata.orientation = deviceOrientationToExifOrienation(orientation);
				
				[shotPictures addObject:image];
				[image release];
				
				if([delegate respondsToSelector:@selector(shotPicture:withIndex:)])
					[delegate shotPicture:image withIndex:nextPicture];
				 
				nextPicture++;
				[self shootNext];
			}];
	} else {
		[self finishShooting];
	}
}

- (void)finishShooting {
	if([delegate respondsToSelector:@selector(shootCompleteWithImages:error:)])
		[delegate shootCompleteWithImages:shotPictures error:nil];
	
	[shotPictures removeAllObjects];
	
	if(totalPictures > 1) {
		if([captureDevice lockForConfiguration:nil]) {
			[self safeSetFocusMode:prevFocusMode];
			[self safeSetExposureMode:prevExposureMode];
			[captureDevice unlockForConfiguration];
		}
	}
}

- (BOOL)isShooting {
	return nextPicture < totalPictures;
}

- (CGSize)dimensions {
	for(AVCaptureInputPort *port in input.ports) {
		if(port.mediaType == AVMediaTypeVideo) {
			CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(port.formatDescription);
			return CGSizeMake(dimensions.width, dimensions.height);
		}
	}
	
	return CGSizeZero;
}

- (CGRect)videoPreviewFrame {
	CGRect dimensions;
	AVCaptureVideoOrientation previewOrientation = previewLayer.orientation;
	if((previewOrientation == AVCaptureVideoOrientationPortrait) ||
	   (previewOrientation == AVCaptureVideoOrientationPortraitUpsideDown)) {
		dimensions = CGRectMake(0.0f, 0.0f, self.dimensions.height, self.dimensions.width);
	} else {
		dimensions = CGRectMake(0.0f, 0.0f, self.dimensions.width, self.dimensions.height);
	}

	
	if(previewLayer.videoGravity == AVLayerVideoGravityResize) {
		return previewLayer.frame;
	} else if(previewLayer.videoGravity == AVLayerVideoGravityResizeAspect) {
		return fitRectInRect(dimensions, previewLayer.frame);
	} else {
		CGRect fit = fitRectInRect(previewLayer.frame, dimensions);
		float scale = previewLayer.frame.size.width / fit.size.width;
		
		CGRect frame;
		frame.size.width = dimensions.size.width * scale;
		frame.size.height = dimensions.size.height * scale;
		frame.origin.x = CGRectGetMidX(previewLayer.frame) - (frame.size.width / 2);
		frame.origin.y = CGRectGetMidY(previewLayer.frame) - (frame.size.height / 2);
		return frame;
	}
}

- (void)setFocusPoint:(CGPoint)p mode:(AVCaptureFocusMode)mode {
	if([captureDevice lockForConfiguration:nil]) {
		if(captureDevice.focusPointOfInterestSupported)
			captureDevice.focusPointOfInterest = p;
		[self safeSetFocusMode:mode];
		[captureDevice unlockForConfiguration];
	}
	
}

- (void)setExposurePoint:(CGPoint)p mode:(AVCaptureExposureMode)mode {
	if([captureDevice lockForConfiguration:nil]) {
		if(captureDevice.exposurePointOfInterestSupported)
			captureDevice.exposurePointOfInterest = p;
		[self safeSetExposureMode:mode];
		[captureDevice unlockForConfiguration];
	}
}

- (BOOL)focusPointSupported {
	return captureDevice.focusPointOfInterestSupported;
}

- (BOOL)exposurePointSupported {
	return captureDevice.exposurePointOfInterestSupported;
}

- (void)safeSetFocusMode:(AVCaptureFocusMode)mode {
	if([captureDevice respondsToSelector:@selector(setFocusMode:)] &&
	   [captureDevice isFocusModeSupported:mode])
	{
		captureDevice.focusMode = mode;
	}
}

- (void)safeSetExposureMode:(AVCaptureExposureMode)mode {
	if([captureDevice respondsToSelector:@selector(setExposureMode:)] &&
	   [captureDevice isExposureModeSupported:mode])
	{
		captureDevice.exposureMode = mode;
	}
}

- (void)setFlash:(BOOL)on {
	if(captureDevice.hasFlash && [captureDevice lockForConfiguration:nil]) {
		captureDevice.flashMode = on ? AVCaptureFlashModeOn : AVCaptureFlashModeOff;
		[captureDevice unlockForConfiguration];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	if([delegate respondsToSelector:@selector(focusStatusChanged:)])
		[delegate focusStatusChanged:[[change valueForKey:NSKeyValueChangeNewKey] boolValue]];
}

- (void) dealloc {
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	
	[captureSession stopRunning];
	[captureSession release];
	[input release];
	[previewLayer release];
	[shotPictures release];
	[output release];
	[super dealloc];
}

@end
