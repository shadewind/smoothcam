//
//  MultiImageOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-23.
//  Copyright 2010 N/A. All rights reserved.
//

#import "MultiImageOperation.h"


@implementation MultiImageOperation

@synthesize output, key, currentImageIndex = currentImage, totalImages;

- (id)init {
	self = [super init];
	if(self) {
		inputImages = [[NSMutableArray alloc] initWithCapacity:10];
	}
	
	return self;
}

- (void)addImage:(Image *)image withKey:(NSString *)key {
	[inputImages addObject:image];
	totalImages++;
}

- (void)addImages:(NSArray *)images {
	[inputImages addObjectsFromArray:images];
	totalImages += [images count];
}

- (void)main {
	if([self isCancelled])
		return;
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[self setup:[inputImages lastObject]];
	
	while([inputImages count] > 0) {
		if([self isCancelled])
			break;
		
		NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
		[self processImage:[inputImages objectAtIndex:0]];
		[inputImages removeObjectAtIndex:0];
		[loopPool drain];
		currentImage++;
	}
	
	if(![self isCancelled])
		[self finish];
	[pool drain];
}

- (void)setup:(Image *)firstImage {
}

- (void)processImage:(Image *)image {
}

- (void)finish {
}

- (float)completion {
	if([self isExecuting])
		return (float)currentImage / totalImages;
	else
		return [super completion];
}

- (void)dealloc {
	[inputImages release];
	self.output = nil;
	self.key = nil;
	[super dealloc];
}

@end
