//
//  ImageTilingLayer.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import "ImageTilingLayer.h"

@implementation ImageTilingLayer

@synthesize image, lowresImage;

- (id)init {
	self = [super init];
	if(self) {
		fastLayer = [CALayer layer];
		fastLayer.contentsScale = 1.0f / 8.0f;
		fastLayer.delegate = self;
		[self addSublayer:fastLayer];
		
		tiledLayer = [CATiledLayer layer];
		tiledLayer.levelsOfDetail = 4;
		tiledLayer.tileSize = CGSizeMake(512.0f, 512.0f);
		tiledLayer.delegate = self;
		[self addSublayer:tiledLayer];
	}
	
	return self;
}

- (void)setImage:(UIImage *)img {
	[image release];
	image = img;
	[image retain];
	
	[tiledLayer setNeedsDisplay];
}

- (void)setLowresImage:(UIImage *)img {
	[lowresImage release];
	lowresImage = img;
	[lowresImage retain];
	
	[fastLayer setNeedsDisplay];
}

- (void)layoutSublayers {
	fastLayer.frame = self.bounds;
	tiledLayer.frame = self.bounds;
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
	UIGraphicsPushContext(ctx);
	CGContextSetInterpolationQuality(ctx, kCGInterpolationNone);
	
	if((layer == fastLayer) && (lowresImage != nil)) {
		CGContextScaleCTM(ctx,image.size.width / lowresImage.size.width,
						  image.size.height / lowresImage.size.height);
		[lowresImage drawAtPoint:CGPointZero];
	} else {
		[image drawAtPoint:CGPointZero];
	}
	
	UIGraphicsPopContext();
}

- (void)dealloc {
	[image release];
	[lowresImage release];
	[super dealloc];
}

@end
