//
//  SaveImageOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SaveImageOperation : NSObject {
	ImageOutput *imageOutput;
}

- (id)initWithImageOuput:(id<ImageOutput>)output;

@end
