//
//  JobQueue.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "JobQueue.h"

#import <AssetsLibrary/AssetsLibrary.h>

#import "Job.h"
#import "GenericImage.h"

@interface JobQueue ()
- (void)finishJob:(Job *)job;
- (void)nextJob;
@end

@implementation JobQueue

@synthesize delegate;

- (id)init {
	self = [super init];
	if(self) {
		jobs = [[NSMutableArray alloc] initWithCapacity:3];
		jobQueue = [[NSOperationQueue alloc] init];
		[jobQueue setMaxConcurrentOperationCount:1];
		operationQueue = [[NSOperationQueue alloc] init];
		taskId = UIBackgroundTaskInvalid;
	}
	
	return self;
}

- (void)addJob:(Job *)job {
	@synchronized(jobs) {
		[jobs addObject:job];
		if([delegate respondsToSelector:@selector(jobAdded:)])
			[delegate jobAdded:job];
		
		[self nextJob];
	}
}

- (void)cancelJob:(Job *)job {
	@synchronized(jobs) {
		if(job != runningJob) {
			[jobs removeObject:job];
			if([delegate respondsToSelector:@selector(jobFinished:)])
				[delegate jobFinished:job];
		}
		[job cancel];
	}
}

- (void)addSingleOperation:(NSOperation *)operation {
	[operationQueue addOperation:operation];
}
										
- (void)finishJob:(Job *)job {
	@synchronized(jobs) {
		runningJob = nil;
		if([delegate respondsToSelector:@selector(jobFinished:)])
			[delegate jobFinished:job];
		if(!job.cancelled) {
			UIApplication *app = [UIApplication sharedApplication];
			UILocalNotification *notification = [[UILocalNotification alloc] init];
			notification.soundName = UILocalNotificationDefaultSoundName;
			notification.applicationIconBadgeNumber = app.applicationIconBadgeNumber + 1;
			[app presentLocalNotificationNow:notification];
			[notification release];
		}
		
		[jobs removeObject:job];
		[self nextJob];
	}
}

- (void)nextJob {
	if(runningJob == nil) {
		if([jobs count] > 0) {
			if(taskId == UIBackgroundTaskInvalid) {
				taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
					[[UIApplication sharedApplication] endBackgroundTask:taskId];
					taskId = UIBackgroundTaskInvalid;
				}];
			}
			runningJob = [jobs objectAtIndex:0];
			
			[runningJob setCompletionBlock:^{
				[self performSelectorOnMainThread:@selector(finishJob:) withObject:runningJob waitUntilDone:NO];
			}];
			if([delegate respondsToSelector:@selector(jobStarted:)])
				[delegate jobStarted:runningJob];
			[runningJob startWithOperationQueue:jobQueue];
		} else if(taskId != UIBackgroundTaskInvalid) {
			[[UIApplication sharedApplication] endBackgroundTask:taskId];
			taskId = UIBackgroundTaskInvalid;
		}
	}
}

- (NSArray *)currentJobs {
	@synchronized(jobs) {
		return [[jobs copy] autorelease];
	}
}

- (void)dealloc {
	[jobs release];
	[jobQueue release];
	[operationQueue release];
	[super dealloc];
}

@end
