//
//  ImageProducer.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-16.
//  Copyright 2010 N/A. All rights reserved.
//

#import "ImageDestination.h"

@protocol ImageProducer < NSObject >

@property (retain, nonatomic) id<ImageDestination> output;
@property (retain, nonatomic) NSString *key;

@end
