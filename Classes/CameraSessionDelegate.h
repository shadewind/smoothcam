//
//  CameraSessionDelegate.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreMedia/CoreMedia.h>

@class Image;

@protocol CameraSessionDelegate <NSObject>

@optional
- (void)shootCompleteWithImages:(NSArray *)images error:(NSError *)error;
- (void)shotPicture:(Image *)picture withIndex:(int)i;
- (void)focusStatusChanged:(BOOL)isFocusing;

@end
