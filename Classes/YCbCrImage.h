//
//  YCbCrImage.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <CoreMedia/CoreMedia.h>

#import "Image.h"

@interface YCbCrImage : Image {
	int width;
	int height;
	NSData *yPlane;
	int yStride;
	NSData *cbcrPlane;
	int cbcrStride;
}

@property (readonly, nonatomic) int width;
@property (readonly, nonatomic) int height;
@property (readonly, nonatomic) NSData *yPlane;
@property (readonly, nonatomic) int yStride;
@property (readonly, nonatomic) NSData *cbcrPlane;
@property (readonly, nonatomic) int cbcrStride;

- (id)initWithSampleBuffer:(CMSampleBufferRef)buffer id:(NSString *)imgId;

- (void)drawY:(CGContextRef)context;
- (void)drawCb:(CGContextRef)context;
- (void)drawCr:(CGContextRef)context;

@end
