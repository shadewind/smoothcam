//
//  SmoothCamViewController.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-13.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CameraSessionDelegate.h"

@class CameraView, CameraSession, JobQueue, QueueViewController, SettingsViewController, TextLayer, GalleryViewController;

@interface SmoothCamViewController : UIViewController <CameraSessionDelegate> {
	SettingsViewController *settingsViewController;
	GalleryViewController *galleryViewController;
	
	CameraView *cameraView;
	NSArray *rotateViews;
	UIButton *shutterButton;
	CALayer *flashLayer;
	TextLayer *pictureNumberLayer;
	CALayer *timerLayer;
	TextLayer *countdownLayer;
	int currentPhoto;
	CALayer *focusPointLayer;
	CALayer *exposurePointLayer;
	CALayer *clearButtonLayer;
	
	NSTimer *timer;
	int secondsLeft;
	CameraSession *cameraSession;
	int numPictures;
	JobQueue *jobQueue;
}

- (IBAction)trigger;
- (IBAction)shoot;
- (IBAction)openGallery;
- (IBAction)openSettings;

@property (nonatomic, retain) IBOutlet CameraView *cameraView;
@property (nonatomic, retain) IBOutlet UIButton *shutterButton;
@property (nonatomic, retain) IBOutletCollection(UIView) NSArray *rotateViews;

@end
