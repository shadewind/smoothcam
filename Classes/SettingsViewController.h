//
//  SettingsViewController.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-17.
//  Copyright 2010 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	kShootingModeSingle = 0,
	kShootingModeMulti = 1,
	kShootingModeMerge = 2
} ShootingMode;

typedef enum {
	kTriggerModeInstant = 0,
	kTriggerModeTimer = 1
} TriggerMode;

@class SteppedSlider;

@interface SettingsViewController : NSObject {
	CGPoint origin;
	CGFloat padding;
	CGSize size;
	BOOL visible;
	
	UIInterfaceOrientation orientation;
	UIView *view;
	UIButton *singleButton;
	UIButton *multiButton;
	UIButton *mergeButton;
	UIButton *instantButton;
	UIButton *timer3Button;
	UIButton *timer10Button;
	UIButton *flashButton;
	UIButton *noflashButton;
	
	BOOL flashOn;
	ShootingMode shootingMode;
	TriggerMode triggerMode;
	int timerDelay;
}

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGFloat padding;
@property (nonatomic) UIInterfaceOrientation orientation;

@property (nonatomic) BOOL flashOn;
@property (nonatomic) ShootingMode shootingMode;
@property (nonatomic) TriggerMode triggerMode;
@property (nonatomic) int timerDelay;

@property (retain, nonatomic) IBOutlet UIView *view;
@property (retain, nonatomic) IBOutlet UIButton *singleButton;
@property (retain, nonatomic) IBOutlet UIButton *multiButton;
@property (retain, nonatomic) IBOutlet UIButton *mergeButton;
@property (retain, nonatomic) IBOutlet UIButton *instantButton;
@property (retain, nonatomic) IBOutlet UIButton *timer3Button;
@property (retain, nonatomic) IBOutlet UIButton *timer10Button;
@property (retain, nonatomic) IBOutlet UIButton *flashButton;
@property (retain, nonatomic) IBOutlet UIButton *noflashButton;

- (IBAction)toggle;
- (IBAction)show;
- (IBAction)hide;
- (IBAction)shootingModeChanged:(id)sender;
- (IBAction)triggerModeChanged:(id)sender;
- (IBAction)flashOnChanged:(id)sender;
- (IBAction)resetDefaults;

@end
