//
//  ConvertImagesOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MultiImageOperation.h"

@interface ConvertImagesOperation : MultiImageOperation {
}

@end
