//
//  TilingImageView.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import "TilingImageView.h"

#import "ImageTilingLayer.h"

@implementation TilingImageView


+ (Class)layerClass {
	return [ImageTilingLayer class];
}

- (void)setImage:(UIImage *)image {
	[(ImageTilingLayer *)self.layer setImage:image];
}

- (UIImage *)image {
	return [(ImageTilingLayer *)self.layer image];
}

- (void)setLowresImage:(UIImage *)image {
	[(ImageTilingLayer *)self.layer setLowresImage:image];
}

- (UIImage *)lowresImage {
	return [(ImageTilingLayer *)self.layer lowresImage];
}

@end
