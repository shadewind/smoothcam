//
//  ImageMetadata.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-27.
//  Copyright 2010 N/A. All rights reserved.
//

#import "ImageMetadata.h"

#import <ImageIO/ImageIO.h>

@implementation ImageMetadata

@synthesize properties, pointsOfInterest;

- (id)init {
	self = [super init];
	if(self) {
		properties = [[NSMutableDictionary alloc] init];
		pointsOfInterest = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (void)setProperty:(id)object forKey:(NSString *)key {
	[properties setObject:object forKey:key];
}

- (void)addPropertiesFrom:(NSDictionary *)dict {
	[properties addEntriesFromDictionary:dict];
}

- (id)propertyForKey:(NSString *)key {
	return [properties objectForKey:key];
}

- (void)addPointOfInterest:(CGPoint)p {
	[pointsOfInterest addObject:[NSValue valueWithCGPoint:p]];
}

- (EXIFOrientation)orientation {
	return orientation;
}

- (void)setOrientation:(EXIFOrientation)o {
	orientation = o;
	[properties setObject:[NSNumber numberWithInt:o] forKey:(NSString *)kCGImagePropertyOrientation];
}

- (void)dealloc {
	[properties release];
	[pointsOfInterest release];
	[super dealloc];
}

@end
