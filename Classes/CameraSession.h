//
//  CameraSession.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>

#import "CameraSessionDelegate.h"

typedef enum {
	kOutputFormatRGBA,
	kOutputFormatYCbCr
} OutputFormat;

@interface CameraSession : NSObject {
	AVCaptureSession *captureSession;
	AVCaptureDevice *captureDevice;
	AVCaptureDeviceInput *input;
	AVCaptureStillImageOutput *output;
	OutputFormat outputFormat;
	AVCaptureConnection *connection;
	AVCaptureVideoPreviewLayer *previewLayer;
	
	AVCaptureFocusMode prevFocusMode;
	AVCaptureExposureMode prevExposureMode;
	NSMutableArray *shotPictures;
	int totalPictures;
	int nextPicture;
	int nextId;
	UIDeviceOrientation orientation;
	
	id<CameraSessionDelegate> delegate;
}

@property (assign, nonatomic) id<CameraSessionDelegate> delegate;
@property (readonly, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property (readonly) CGSize dimensions;
@property (readonly) CGRect videoPreviewFrame;
@property (nonatomic) OutputFormat outputFormat;
@property (nonatomic, readonly) BOOL focusPointSupported;
@property (nonatomic, readonly) BOOL exposurePointSupported;
@property (nonatomic) UIDeviceOrientation orientation;

- (void)shootPictures:(int)n;
- (BOOL)isShooting;
- (void)setFocusPoint:(CGPoint)p mode:(AVCaptureFocusMode)mode;
- (void)setExposurePoint:(CGPoint)p mode:(AVCaptureExposureMode)mode;
- (void)setFlash:(BOOL)on;

@end