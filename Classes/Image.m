//
//  Image.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-20.
//  Copyright 2010 N/A. All rights reserved.
//

#import "Image.h"

#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "GenericImage.h"
#import "imageprocessing.h"
#import "uiutils.h"

@implementation Image

@synthesize imageId, imageTransform, metadata;

- (id)initWithId:(NSString *)imgId {
	self = [super init];
	if(self) {
		imageId = imgId;
		[imageId retain];
		imageTransform = CGAffineTransformIdentity;
		transformStack = [[NSMutableArray alloc] init];
		metadata = [[ImageMetadata alloc] init];
	}
	
	return self;
}

- (CGRect)bounds {
	return CGRectMake(0, 0, self.width, self.height);
}

-(CGSize)size {
	return CGSizeMake(self.width, self.height);
}

- (void)saveTransform {
	[transformStack addObject:[NSValue valueWithCGAffineTransform:imageTransform]];
}

- (void)restoreTransform {
	imageTransform = [[transformStack lastObject] CGAffineTransformValue];
	[transformStack removeLastObject];
}

- (void)transform:(CGAffineTransform)transform {
	imageTransform = CGAffineTransformConcat(imageTransform, transform);
}

- (UIImage *)previewThumbnail {
	CGSize size = CGSizeMake(100.0f, 100.0f);
	CGContextRef context = createRgbaBitmap(size.width, size.height);
	CGContextSetInterpolationQuality(context, kCGInterpolationNone);
	CGFloat scaleX = size.width / self.width;
	CGFloat scaleY = size.height / self.height;
	CGFloat scale = MAX(scaleX, scaleY);
	CGContextScaleCTM(context, scale, scale);
	CGSize scaledSize = CGSizeApplyAffineTransform(self.size, CGAffineTransformMakeScale(scale, scale));
	CGContextTranslateCTM(context, (size.width - scaledSize.width) / 2.0f, (size.height - scaledSize.height) / 2.0f);
	[self drawPreview:context];
	CGImageRef image = CGBitmapContextCreateImage(context);
	CFRelease(context);
	UIImage *uiImage = [UIImage imageWithCGImage:image
										   scale:1.0f
									 orientation:exifOrientationToUIImageOrientation(self.metadata.orientation)];
	CFRelease(image);
	return uiImage;
}

- (BOOL)lock {
	@synchronized(self) {
		if(file != nil) {
			if([self loadFromFile:file]) {
				[[NSFileManager defaultManager] removeItemAtURL:file error:nil];
				[file release];
				file = nil;
			}
			else {
				return NO;
			}
		}
		
		lockCount++;
		return YES;
	}
}

- (void)unlock {
	@synchronized(self) {
		lockCount--;
	}
}

- (void)unloadImage {
	@synchronized(self) {
		if(lockCount == 0) {
			file = [self unloadToFile];
			[file retain];
		}
	}
}

- (void)dealloc {
	if(file) {
		[[NSFileManager defaultManager] removeItemAtURL:file error:nil];
		[file release];
	}
	self.metadata = nil;
	[imageId release];
	[transformStack release];
	[super dealloc];
}

#pragma mark Dummy implementations

- (int)width {
	return 0;
}

- (int)height {
	return 0;
}

- (CGImageRef)createCGImage {
	return NULL;
}

- (void)drawPreview:(CGContextRef)context {
}

- (GenericImage *)genericImage {
	return nil;
}

- (GenericImage *)grayscaleImage {
	return nil;
}

- (NSURL *)unloadToFile {
	return nil;
}

- (BOOL)loadFromFile:(NSURL *)file {
	return NO;
}

@end
