//
//  ImageOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-17.
//  Copyright 2010 N/A. All rights reserved.
//

#import "ImageOperation.h"

@implementation ImageOperation

@synthesize cost;

+ (id)operation {
	return [[[self alloc] init] autorelease];
}

- (id)init {
	self = [super init];
	if(self) {
		cost = 1.0f;
	}
	
	return self;
}

- (NSString *)progressString {
	return @"Processing...";
}

- (float)completion {
	if([self isFinished])
		return 1.0f;
	else
		return 0.0f;
}

@end
