/*
 *  Image.m
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-07.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

#import "GenericImage.h"

#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>

#import "YCbCrImage.h"
#import "imageprocessing.h"


@implementation GenericImage

@synthesize width, height;

- (id)initWithSampleBuffer:(CMSampleBufferRef)buffer id:(NSString *)imgId {
	CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(buffer);
	CVPixelBufferLockBaseAddress(pixelBuffer, 0);
	NSData *data = [NSData dataWithBytes:CVPixelBufferGetBaseAddress(pixelBuffer)
								  length:CVPixelBufferGetDataSize(pixelBuffer)];
	CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)data);
	CGImageRef cgImage = CGImageCreate(CVPixelBufferGetWidth(pixelBuffer),
									   CVPixelBufferGetHeight(pixelBuffer),
									   8, 32,
									   CVPixelBufferGetBytesPerRow(pixelBuffer),
									   rgbColorspace(),
									   kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst,
									   dataProvider, NULL, true, kCGRenderingIntentDefault);
	CFRelease(dataProvider);
	self = [self initWithCGImage:cgImage id:imgId];
	CFRelease(cgImage);
	return self;
}

- (id)initWithCGImage:(CGImageRef)cgImage id:(NSString *)imgId {
	self = [super initWithId:imgId];
	if(self) {
		image = cgImage;
		CFRetain(image);
		
		width = CGImageGetWidth(image);
		height = CGImageGetHeight(image);
	}
	
	return self;
}

- (void)drawTo:(CGContextRef)context {
	if([self lock]) {
		CGContextSaveGState(context);
		CGContextConcatCTM(context, self.imageTransform);
		CGContextDrawImage(context, self.bounds, image);
		CGContextRestoreGState(context);
		[self unlock];
	}
}

- (BOOL)isGrayscale {
	if([self lock]) {
		BOOL ret = CGImageGetBitsPerPixel(image) == 8;
		[self unlock];
		return ret;
	}
	
	return NO;
}

- (CGContextRef)createCompatibleBitmapWithWidth:(int)w height:(int)h {
	if(w == 0)
		w = width;
	if(h == 0)
		h = height;
	
	if([self isGrayscale])
		return createGrayscaleBitmap(w, h);
	else
		return createRgbaBitmap(w, h);
}

- (GenericImage *)genericImage {
	return self;
}

- (GenericImage *)grayscaleImage {
	if([self lock]) {
		CGImageRef cgGray = createGrayscale(image);
		GenericImage *grayscale = [[GenericImage alloc] initWithCGImage:cgGray id:self.imageId];
		CFRelease(cgGray);
		[self unlock];
		return [grayscale autorelease];
	}
	
	return nil;
}

- (CGContextRef)createBitmap:(CGRect)rect {
	if([self lock]) {
		CGContextRef context = [self createCompatibleBitmapWithWidth:rect.size.width height:rect.size.height];
		CGContextSetInterpolationQuality(context, kCGInterpolationLow);
		CGContextSaveGState(context);
		CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
		[self drawTo:context];
		CGContextRestoreGState(context);
		[self unlock];
		return context;
	}
	
	return NULL;
}

- (CGContextRef)createScaledBitmap:(CGRect)rect withScale:(float)scale {
	if([self lock]) {
		CGAffineTransform scaleTransform = CGAffineTransformMakeScale(scale, scale);
		CGRect scaledRect = CGRectApplyAffineTransform(rect, scaleTransform);
		
		CGContextRef context = [self createCompatibleBitmapWithWidth:CGRectGetWidth(scaledRect) height:CGRectGetHeight(scaledRect)];
		CGContextSaveGState(context);
		CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
		CGContextConcatCTM(context, scaleTransform);
		CGContextDrawImage(context, self.bounds, image);
		CGContextRestoreGState(context);
		[self unlock];
		
		return context;
	}
	
	return NULL;
}

- (CGImageRef)createCGImage {
	if([self lock]) {
		CGImageRef ret;
		if(CGAffineTransformIsIdentity(self.imageTransform)) {
			ret = image;
			CFRetain(image);
		} else {
			CGContextRef bitmap = [self createBitmap:CGRectMake(0.0f, 0.0f, width, height)];
			ret = CGBitmapContextCreateImage(bitmap);
			CFRelease(bitmap);
		}
		
		return ret;
	}
	
	return NULL;
}

- (void)drawPreview:(CGContextRef)context {
	[self drawTo:context];
}

- (BOOL)loadFromFile:(NSURL *)file {
	NSData *data = [NSData  dataWithContentsOfURL:file];
	CGImageSourceRef imageSource = CGImageSourceCreateWithData((CFDataRef)data, NULL);
	image = CGImageSourceCreateImageAtIndex(imageSource, 0, NULL);
	CFRelease(imageSource);
	return image != NULL;
}

- (NSURL *)unloadToFile {
	NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:
					  [NSString stringWithFormat:@"rgba_%@.tiff", self.imageId]];
	NSURL *file = [NSURL fileURLWithPath:path];
	
	CGImageDestinationRef imageDest = CGImageDestinationCreateWithURL((CFURLRef)file,
																	  kUTTypeTIFF, 1, NULL);
	CGImageDestinationAddImage(imageDest, image, NULL);
	CGImageDestinationFinalize(imageDest);
	CFRelease(imageDest);
	
	CFRelease(image);
	image = NULL;
	return file;
}

- (void)dealloc {
	if(image)
		CFRelease(image);
	[super dealloc];
}

@end