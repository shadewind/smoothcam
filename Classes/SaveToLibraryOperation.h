//
//  SaveToLibraryOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-23.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "MultiImageOperation.h"

@interface SaveToLibraryOperation : MultiImageOperation {
	NSString *format;
	ALAssetsLibrary *library;
	NSConditionLock *lock;
	NSMutableArray *assetUrls;
}

@property (nonatomic, retain) NSString *format;
@property (readonly, nonatomic) NSArray *assetUrls;

@end
