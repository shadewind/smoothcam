/*
 *  GenericImage.h
 *  imgalign
 *
 *  Created by Emil Eriksson on 2010-12-07.
 *  Copyright 2010 N/A. All rights reserved.
 *
 */

#import <TargetConditionals.h>

#import <CoreGraphics/CoreGraphics.h>

#import "Image.h"
#import "utils.h"

@class YCbCrImage;

@interface GenericImage : Image {
	CGImageRef image;
	int width;
	int height;
}

@property (readonly, nonatomic) int width;
@property (readonly, nonatomic) int height;

- (id)initWithCGImage:(CGImageRef)cgImage id:(NSString *)imgId;
- (CGImageRef)createCGImage;
- (void)drawTo:(CGContextRef)context;
- (BOOL)isGrayscale;
- (CGContextRef)createCompatibleBitmapWithWidth:(int)w height:(int)h;
- (CGContextRef)createBitmap:(CGRect)rect;
- (CGContextRef)createScaledBitmap:(CGRect)rect withScale:(float)scale;

@end