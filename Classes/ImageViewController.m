//
//  ImageViewController.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-14.
//  Copyright 2011 N/A. All rights reserved.
//

#import "ImageViewController.h"

#import <ImageIO/ImageIO.h>

#import "ImageScrollView.h"

@interface ImageViewController ()
- (void)switchToIndex:(int)idx;
- (void)toggleZoom:(UIGestureRecognizer *)recognizer;
- (void)toggleBars;
- (void)releaseView;
@end

@implementation ImageViewController

@synthesize urls, index;

- (id)init {
	self = [super init];
	if(self) {
		library = [[ALAssetsLibrary alloc] init];
	}
	
	return self;
}

- (void)loadView {
	UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
	view.backgroundColor = [UIColor blackColor];
	self.view = view;
	[view release];
	
	scrollView = [[ImageScrollView alloc] initWithFrame:view.bounds];
	scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[view addSubview:scrollView];
	
	UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZoom:)];
	doubleTap.numberOfTapsRequired = 2;
	[scrollView addGestureRecognizer:doubleTap];
	[doubleTap release];
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleBars)];
	singleTap.numberOfTapsRequired = 1;
	[singleTap requireGestureRecognizerToFail:doubleTap];
	[scrollView addGestureRecognizer:singleTap];
	[singleTap release];
}

- (void)viewWillAppear:(BOOL)animated {
	[self switchToIndex:index];
	[self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	CGPoint savedCenter = [scrollView centerPoint];
	float savedScale = scrollView.zoomScale;
	float savedMin = scrollView.minimumZoomScale;
	
	[scrollView updateZoomSettings];
	if(savedScale == savedMin) {
		scrollView.zoomScale = scrollView.minimumZoomScale;
	} else {
		[scrollView zoomToPoint:savedCenter scale:savedScale];
	}
}

- (void)toggleZoom:(UIGestureRecognizer *)recognizer {
	if(scrollView.zoomScale > scrollView.minimumZoomScale) {
		[scrollView setZoomScale:scrollView.minimumZoomScale animated:YES];
	} else {
		[scrollView zoomInToPoint:[recognizer locationInView:scrollView]];
	}
}

- (void)toggleBars {
	UINavigationController *navController = self.navigationController;
	[navController setNavigationBarHidden:!navController.navigationBarHidden animated:YES];
}

- (void)switchToIndex:(int)idx {
	ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset *asset) {
		ALAssetRepresentation *representation = [asset defaultRepresentation];
		
		NSDictionary *options = [NSDictionary dictionaryWithObject:(id)kCFBooleanTrue forKey:(id)kCGImageSourceShouldCache];
		UIImage *image = [UIImage imageWithCGImage:[representation CGImageWithOptions:options]
											 scale:[representation scale]
									   orientation:[representation orientation]];
		
		UIImage *lowresImage = [UIImage imageWithCGImage:[representation fullScreenImage]
												   scale:[representation scale]
											 orientation:[representation orientation]];
		
		[scrollView displayImage:image lowRes:lowresImage];
		index = idx;
	};
	
	[library assetForURL:[urls objectAtIndex:idx] resultBlock:resultBlock failureBlock:nil];
}

- (void)releaseView {
	[scrollView release];
}

- (void)viewDidUnload {
	[self releaseView];
}

- (void)dealloc {
	[self releaseView];
	[library release];
	self.urls = nil;
	
	[super dealloc];
}

@end
