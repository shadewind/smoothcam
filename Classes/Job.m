//
//  Job.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "Job.h"

#import <MobileCoreServices/MobileCoreServices.h>

#import "Image.h"
#import "ImageOperation.h"
#import "ImageProducer.h"
#import "ImageDestination.h"
#import "SaveToLibraryOperation.h"
#import "uiutils.h"

@interface Job ()
- (void)addOperation:(ImageOperation *)operation;
- (void)finish;
@end

@implementation Job

@synthesize description, thumbnail, cancelled, outputFormat;

+ (Job *)job {
	return [[[Job alloc] init] autorelease];
}

- (id)init {
	self = [super init];
	if(self) {
		operations = [[NSMutableArray alloc] initWithCapacity:3];
		outputFormat = (NSString *)kUTTypeJPEG;
	}
	
	return self;
}

- (void)addOperation:(ImageOperation *)operation {
	[operation setCompletionBlock:^{
		@synchronized(self) {
			if(!finishCalled && [self isFinished]) {
				[self finish];
				finishCalled = YES;
			}
		}
	}];
	
	[operations	addObject:operation];
}

- (void)chainOperation:(ImageOperation *)operation {
	if([operations count] > 0) {
		ImageOperation<ImageProducer> *previousOperation = [operations lastObject];
		previousOperation.output = (id<ImageDestination>)operation;
		[operation addDependency:previousOperation];
	}
	
	[self addOperation:operation];
}

- (void)startWithOperationQueue:(NSOperationQueue *)queue {
	saveOperation = [SaveToLibraryOperation operation];
	saveOperation.format = outputFormat;
	[self chainOperation:saveOperation];
	
	[queue addOperations:operations waitUntilFinished:NO];
	started = YES;
}

- (NSArray *)assetUrls {
	if([self isFinished])
		return saveOperation.assetUrls;
	else
		return [NSArray array];
}


- (void)finish {
	completionBlock();
}

- (BOOL)isFinished {
	for(NSOperation *operation in operations) {
		if(![operation isFinished])
			return NO;
	}
	
	return YES;
}

- (void)setCompletionBlock:(void (^)(void))block {
	[completionBlock release];
	completionBlock = [block copy];
}

- (float)completion {
	float totalCost = 0.0f;
	float completedCost = 0.0f;
	for(ImageOperation *operation in operations) {
		float cost = operation.cost;
		totalCost += cost;
		completedCost += (cost * [operation completion]);
	}
	
	return completedCost / totalCost;
}

- (NSString *)progressDescription {
	if(cancelled)
		return @"Cancelling...";
	
	for(ImageOperation *operation in operations) {
		if([operation isExecuting])
			return [operation progressString];
	}
	
	return @"Processing...";
}

- (BOOL)isRunning {
	return started && !finishCalled;
}

- (void)cancel {
	cancelled = YES;
	for(ImageOperation *operation in operations)
		[operation cancel];
}
		
- (void)dealloc {
	self.description = nil;
	self.thumbnail = nil;
	self.outputFormat = nil;
	[operations release];
	[completionBlock release];
	[super dealloc];
}

@end
