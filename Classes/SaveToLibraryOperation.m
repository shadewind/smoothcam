//
//  SaveToLibraryOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-23.
//  Copyright 2010 N/A. All rights reserved.
//

#import "SaveToLibraryOperation.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "GenericImage.h"

@implementation SaveToLibraryOperation

@synthesize format, assetUrls;

- (id)init {
	self = [super init];
	if(self) {
		self.cost = 0.1;
		format = (NSString *)kUTTypeJPEG;
		lock = [[NSConditionLock alloc] initWithCondition:0];
		library = [[ALAssetsLibrary alloc] init];
		assetUrls = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (void)processImage:(Image *)image {
	[lock lockWhenCondition:0];
	
	CGImageRef cgImage = [image createCGImage];
	NSMutableData *data = [NSMutableData data];
	CGImageDestinationRef imageDest = CGImageDestinationCreateWithData((CFMutableDataRef)data, (CFStringRef)format, 1, NULL);
	CGImageDestinationAddImage(imageDest, cgImage, (CFDictionaryRef)image.metadata.properties);
	CFRelease(cgImage);
	CGImageDestinationFinalize(imageDest);
	CFRelease(imageDest);
	
	[library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:
		^(NSURL *url, NSError *error) {
			[lock lockWhenCondition:1];
			[assetUrls addObject:url];
			[lock unlockWithCondition:0];
		}];

	[lock unlockWithCondition:1];
	
	//Wait until done:
	[lock lockWhenCondition:0];
	[lock unlockWithCondition:0];
}

- (NSString *)progressString {
	return @"Saving to library...";
}

- (void)dealloc {
	[library release];
	[lock release];
	[assetUrls release];
	[super dealloc];
}

@end
