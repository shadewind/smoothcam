//
//  ImageMetadata.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-27.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EXIFOrientationUp = 1,
	EXIFOrientationRight = 8,
	EXIFOrientationDown = 3,
	EXIFOrientationLeft = 6,
	EXIFOrientationUpMirrored = 2,
	EXIFOrientationRightMirrored = 7,
	EXIFOrientationDownMirrored = 4,
	EXIFOrientationLeftMirrored = 5
} EXIFOrientation;

@interface ImageMetadata : NSObject {
	NSMutableDictionary *properties;
	NSMutableArray *pointsOfInterest;
	EXIFOrientation orientation;
}

@property (readonly, nonatomic) NSDictionary *properties;
@property (readonly, nonatomic) NSArray *pointsOfInterest;
@property (nonatomic) EXIFOrientation orientation;

- (void)setProperty:(id)object forKey:(NSString *)key;
- (void)addPropertiesFrom:(NSDictionary *)dict;
- (id)propertyForKey:(NSString *)key;

- (void)addPointOfInterest:(CGPoint)p;

@end
