//
//  Image.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-20.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageMetadata.h"

@class GenericImage;

@interface Image : NSObject {
@private
	NSString *imageId;
	CGAffineTransform imageTransform;
	NSMutableArray *transformStack;
	ImageMetadata *metadata;
	
	NSURL *file;
	int lockCount;
}

@property (nonatomic, readonly) NSString *imageId;
@property (nonatomic, readonly) CGRect bounds;
@property (nonatomic, readonly) CGSize size;
@property (nonatomic, readonly) int width;
@property (nonatomic, readonly) int height;
@property (nonatomic) CGAffineTransform imageTransform;
@property (nonatomic, retain) ImageMetadata *metadata;

- (id)initWithId:(NSString *)imgId;
- (void)saveTransform;
- (void)restoreTransform;
- (void)transform:(CGAffineTransform)transform;
- (GenericImage *)genericImage;
- (GenericImage *)grayscaleImage;
- (CGImageRef)createCGImage;
- (void)drawPreview:(CGContextRef)context;
- (UIImage *)previewThumbnail;

- (void)unloadImage;
- (BOOL)lock;
- (void)unlock;

- (NSURL *)unloadToFile;
- (BOOL)loadFromFile:(NSURL *)file;

@end
