//
//  TextLayer.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-12.
//  Copyright 2011 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface TextLayer : CALayer {
	NSString *text;
	UIFont *font;
	UIColor *color;
}

@property (retain, nonatomic) NSString *text;
@property (retain, nonatomic) UIFont *font;
@property (retain, nonatomic) UIColor *color;

@end
