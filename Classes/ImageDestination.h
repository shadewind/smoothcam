//
//  ImageDestination.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-16.
//  Copyright 2010 N/A. All rights reserved.
//

@class Image;

@protocol ImageDestination < NSObject >

- (void)addImage:(Image *)image withKey:(NSString *)key;

@end
