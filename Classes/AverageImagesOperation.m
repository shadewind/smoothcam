//
//  AverageImagesOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-16.
//  Copyright 2010 N/A. All rights reserved.
//

#import "AverageImagesOperation.h"

#import "GenericImage.h"
#import "imageprocessing.h"

@implementation AverageImagesOperation

- (void)setup:(Image *)firstImage {
	context = createRgbaBitmap(firstImage.width, firstImage.height);
}

- (void)processImage:(Image *)image {
		CGContextSetAlpha(context, 1.0f / (self.currentImageIndex + 1));
		[(GenericImage *)image drawTo:context];
}

- (void)finish {
	CGImageRef cgImage = CGBitmapContextCreateImage(context);
	CFRelease(context);
	context = NULL;
	GenericImage *genericImage = [[GenericImage alloc] initWithCGImage:cgImage id:@"combined"];
	CFRelease(cgImage);
	[self.output addImage:genericImage withKey:self.key];
	[genericImage release];
}

- (NSString *)progressString {
	return @"Merging images...";
}

- (void)dealloc {
	if(context)
		CFRelease(context);
	[super dealloc];
}

@end
