//
//  Job.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageDestination.h"

@class ImageOperation, SaveToLibraryOperation;

@interface Job : NSObject {
	NSMutableArray *operations;
	SaveToLibraryOperation *saveOperation;
	NSString *outputFormat;
	NSString *description;
	UIImage *thumbnail;
	
	void (^completionBlock)(void);
	BOOL started;
	BOOL finishCalled;
	BOOL cancelled;
}

@property (readonly, nonatomic) NSArray *assetUrls;
@property (nonatomic, retain) NSString *outputFormat;
@property (retain, nonatomic) NSString *description;
@property (retain, nonatomic) UIImage *thumbnail;
@property (nonatomic, readonly) BOOL cancelled;

+ (Job *)job;
- (id)init;
- (void)chainOperation:(ImageOperation *)operation;
- (void)startWithOperationQueue:(NSOperationQueue *)queue;
- (BOOL)isFinished;
- (void)setCompletionBlock:(void (^)(void))block;
- (BOOL)isRunning;
- (float)completion;
- (NSString *)progressDescription;
- (void)cancel;

@end
