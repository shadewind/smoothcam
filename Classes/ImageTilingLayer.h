//
//  ImageTilingLayer.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface ImageTilingLayer : CALayer {
	UIImage *image;
	UIImage *lowresImage;
	CALayer *fastLayer;
	CATiledLayer *tiledLayer;
}

@property (retain, nonatomic) UIImage *lowresImage;
@property (retain, nonatomic) UIImage *image;

@end
