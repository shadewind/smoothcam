//
//  ImageScrollView.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TilingImageView;

@interface ImageScrollView : UIScrollView <UIScrollViewDelegate> {
	TilingImageView *imageView;
}

- (void)displayImage:(UIImage *)image lowRes:(UIImage *)lowresImage;
- (void)updateZoomSettings;
- (void)zoomToPoint:(CGPoint)p scale:(float)scale;
- (CGPoint)centerPoint;
- (void)zoomInToPoint:(CGPoint)p;

@end
