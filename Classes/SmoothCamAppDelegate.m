//
//  SmoothCamAppDelegate.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-13.
//  Copyright 2010 N/A. All rights reserved.
//

#import "SmoothCamAppDelegate.h"

#import "SmoothCamViewController.h"

@interface SmoothCamAppDelegate ()
- (void)cleanTempDirectory;
@end

@implementation SmoothCamAppDelegate

@synthesize window, rootController;

#pragma mark -
#pragma mark Application lifecycle

+ (void)initialize {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSURL *defaultsUrl = [[NSBundle mainBundle] URLForResource:@"Defaults" withExtension:@"plist"];
	NSDictionary *defaultsDict = [NSDictionary dictionaryWithContentsOfURL:defaultsUrl];
	for(NSString *key in defaultsDict) {
		if([defaults objectForKey:key] == nil)
			[defaults setObject:[defaultsDict objectForKey:key] forKey:key];
	}
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[self cleanTempDirectory];
	
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)cleanTempDirectory {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	for(NSString *file in [fileManager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:nil])
		[fileManager removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:file] error:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[application setApplicationIconBadgeNumber:0];
}

- (void)dealloc {
	self.rootController = nil;
    [window release];
    [super dealloc];
}


@end
