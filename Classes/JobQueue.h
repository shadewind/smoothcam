//
//  JobQueue.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JobQueueDelegate.h"

@class ALAssetsLibrary, Job;

@interface JobQueue : NSObject {
	NSMutableArray *jobs;
	Job *runningJob;
	NSOperationQueue *jobQueue;
	NSOperationQueue *operationQueue;
	
	id<JobQueueDelegate> delegate;
	UIBackgroundTaskIdentifier taskId;
}

@property (readonly) NSArray *currentJobs;
@property (assign, nonatomic) id<JobQueueDelegate> delegate;

- (void)addJob:(Job *)job;
- (void)cancelJob:(Job *)job;
- (void)addSingleOperation:(NSOperation *)operation;

@end
