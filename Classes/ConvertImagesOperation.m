//
//  ConvertImageOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "ConvertImagesOperation.h"

#import <CoreVideo/CoreVideo.h>

#import "YCbCrImage.h"
#import "GenericImage.h"
#import "ImageDestination.h"

@implementation ConvertImagesOperation

- (void)processImage:(Image *)image {
	GenericImage *genericImage = [image genericImage];
	[genericImage unloadImage];
	[self.output addImage:genericImage withKey:self.key];
}

- (NSString *)progressString {
	return @"Performing color conversion...";
}
		   
@end
