    //
//  GalleryViewController.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-13.
//  Copyright 2011 N/A. All rights reserved.
//

#import "GalleryViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "Job.h"
#import "JobQueue.h"
#import "settings.h"
#import "ImageViewController.h"

@interface GalleryViewController ()
- (void)layout;
- (void)openImage:(UIGestureRecognizer *)recognizer;
- (void)loadAssetUrl:(NSURL *)url forView:(UIImageView *)view;
- (UIImageView *)createAssetView;
- (void)saveUrls;
- (void)close;
- (void)clearAll;
- (void)releaseView;
@end

@implementation GalleryViewController

@synthesize jobQueue, scrollView, galleryView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
		urls = [[NSMutableArray alloc] init];
		assetViews = [[NSMutableArray alloc] init];
		jobViews = [[NSMutableArray alloc] init];
		library = [[ALAssetsLibrary alloc] init];
		imageViewController = [[ImageViewController alloc] init];
		
		UINavigationItem *navItem = self.navigationItem;
		navItem.title = @"Recent photos";
		UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithTitle:@"Close"
																	  style:UIBarButtonItemStyleDone
																	 target:self
																	 action:@selector(close)];
		navItem.leftBarButtonItem = closeItem;
		[closeItem release];
		UIBarButtonItem *clearItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear"
																	  style:UIBarButtonItemStylePlain
																	 target:self
																	 action:@selector(clearAll)];
		navItem.rightBarButtonItem = clearItem;
		[clearItem release];
		
		NSArray *savedUrls = [[NSUserDefaults standardUserDefaults] arrayForKey:kGalleryUrlsKey];
		if(savedUrls) {
			for(NSString *str in savedUrls)
				[urls addObject:[NSURL URLWithString:str]];
		}
    }
    return self;
}

- (void)viewDidLoad {
	for(NSURL *url in urls)
		[self loadAssetUrl:url forView:[self createAssetView]];
	
	for(Job *job in jobQueue.currentJobs)
		[self jobAdded:job];
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
	[galleryView addGestureRecognizer:singleTap];
	[singleTap release];
}

- (void)viewWillAppear:(BOOL)animated {
	[self layout];
}

- (void)setJobQueue:(JobQueue *)queue {
	[jobQueue release];
	jobQueue.delegate = nil;
	jobQueue = queue;
	jobQueue.delegate = self;
	[jobQueue retain];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[self layout];
}

- (void)jobAdded:(Job *)job {
	if([self isViewLoaded]) {
		UIImageView *jobView = [[UIImageView alloc] initWithImage:job.thumbnail];
		jobView.image = job.thumbnail;
		jobView.alpha = 0.3f;
		[jobViews addObject:jobView];
		[galleryView addSubview:jobView];
		[self layout];
		
		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] 
												 initWithActivityIndicatorStyle:
												 UIActivityIndicatorViewStyleWhite];
		activityView.tag = 1;
		activityView.hidesWhenStopped = NO;
		activityView.center = CGPointMake(CGRectGetWidth(jobView.bounds) / 2.0f,
										  CGRectGetHeight(jobView.bounds) / 2.0f);
		activityView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
	                                    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
		[jobView addSubview:activityView];
		
		[activityView release];
		[jobView release];
		
		if([job isRunning])
			[self jobStarted:job];
	}
}

- (void)jobStarted:(Job *)job {
	if([self isViewLoaded]) {
		int index = [jobQueue.currentJobs indexOfObject:job];
		UIImageView *jobView = [jobViews objectAtIndex:index];
		jobView.alpha = 0.8f;
		UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[jobView viewWithTag:1];
		[activityView startAnimating];
	}
}

- (void)jobFinished:(Job *)job {
	[urls addObjectsFromArray:job.assetUrls];
	[self saveUrls];

	if([self isViewLoaded]) {
		int index = [jobQueue.currentJobs indexOfObject:job];
		UIImageView *jobView = [jobViews objectAtIndex:index];
		for(NSURL *url in job.assetUrls) {
			UIImageView *assetView = [self createAssetView];
			assetView.frame = jobView.frame;
			[self loadAssetUrl:url forView:assetView];
		}
		[jobViews removeObjectAtIndex:index];
		[jobView removeFromSuperview];
		
		[UIView animateWithDuration:0.3f animations:^{ [self layout]; }];
	}
}

- (void)loadAssetUrl:(NSURL *)url forView:(UIImageView *)view {
	ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset *asset) {
		if(asset) {
			view.image = [UIImage imageWithCGImage:[asset thumbnail]];
			view.userInteractionEnabled = YES;
			[UIView animateWithDuration:0.3f
								  delay:0.0f
								options:UIViewAnimationOptionAllowUserInteraction
							 animations:^{ view.alpha = 1.0f; }
							 completion:nil];
		} else {
			[urls removeObject:url];
			[view removeFromSuperview];
			[assetViews removeObject:view];
			[self saveUrls];
		}
	};
	
	[library assetForURL:url resultBlock:resultBlock failureBlock:nil];
}

- (UIImageView *)createAssetView {
	UIImageView *assetView = [[UIImageView alloc] init];
	assetView.alpha = 0.0f;
	[assetViews addObject:assetView];
	[galleryView addSubview:assetView];
	[assetView release];
	return assetView;
}

- (void)openImage:(UIGestureRecognizer *)recognizer {
	CGPoint p = [recognizer locationInView:galleryView];
	UIView *hitView = [galleryView hitTest:p withEvent:nil];
	if(hitView != galleryView) {
		int index = [assetViews indexOfObject:hitView];
		if(index != NSNotFound) {
			imageViewController.urls = urls;
			imageViewController.index = index;
			[self.navigationController pushViewController:imageViewController animated:YES];
		}
	}
}

- (void)layout {
	int imagesPerRow = 3;
	if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
		imagesPerRow = 4;
	
	NSMutableArray *allViews = [NSMutableArray arrayWithArray:assetViews];
	CGFloat spacing = 10.0f;
	CGFloat tileSize = ((CGRectGetWidth(scrollView.bounds) - spacing) / imagesPerRow) - spacing;
	CGFloat yPos = spacing;
	CGFloat xPos = spacing;
	[allViews addObjectsFromArray:jobViews];
	for(UIImageView *view in allViews) {
		if((xPos + tileSize) > CGRectGetWidth(scrollView.bounds)) {
			yPos += (tileSize + spacing);
			xPos = spacing;
		}
		view.frame = CGRectMake(xPos, yPos, tileSize, tileSize);
		xPos += (tileSize + spacing);
	}
	
	CGSize size = CGSizeMake(CGRectGetWidth(scrollView.bounds), (yPos + tileSize + spacing));
	CGPoint savedOffset = scrollView.contentOffset;
	galleryView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
	scrollView.contentSize = size;
	scrollView.contentOffset = savedOffset;
	
	CGFloat navBarHeight = self.navigationController.navigationBar.bounds.size.height;
	UIEdgeInsets insets = UIEdgeInsetsMake(navBarHeight, 0.0f, 0.0f, 0.0f);
	scrollView.contentInset = insets;
	scrollView.scrollIndicatorInsets = insets;
	
}

- (void)saveUrls {
	NSMutableArray *urlStrings = [NSMutableArray array];
	for(NSURL *url in urls)
		[urlStrings addObject:[url absoluteString]];
	[[NSUserDefaults standardUserDefaults] setObject:urlStrings forKey:kGalleryUrlsKey];
}

- (void)close {
	[self dismissModalViewControllerAnimated:YES];
}

- (void)clearAll {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Do you want to clear all photos? The photos "
																	  @"will still remain in your photo album."
															 delegate:self
													cancelButtonTitle:@"Cancel"
											   destructiveButtonTitle:@"Clear all"
													otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
	[actionSheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 0) {
		NSArray *clearViews = [[assetViews copy] autorelease];
		[urls removeAllObjects];
		[self saveUrls];
		[assetViews removeAllObjects];
		
		CGRect toRect = CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
		void (^animation)(void) = ^{
			for(UIImageView *view in clearViews)
				view.frame = toRect;
		};
		
		void (^completion)(BOOL) = ^(BOOL finished) {
			for(UIImageView *view in clearViews)
				[view removeFromSuperview];
		};
		
		[UIView animateWithDuration:0.3f animations:animation completion:completion];
		CGRect scrollRect = CGRectMake(0.0f, 0.0f, scrollView.bounds.size.width, scrollView.bounds.size.height);
		[UIView animateWithDuration:0.3f animations:^{
			[self layout];
			[scrollView scrollRectToVisible:scrollRect animated:NO];
		}];
	}
}

- (void)releaseView {
	self.scrollView = nil;
	self.galleryView = nil;
	[assetViews removeAllObjects];
	[jobViews removeAllObjects];
}

- (void)viewDidUnload {
	[self releaseView];
}

- (void)dealloc {
	[self releaseView];
	
	[urls release];
	[assetViews release];
	[jobViews release];
	[library release];
	[imageViewController release];
	
    [super dealloc];
}


@end
