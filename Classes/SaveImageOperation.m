//
//  SaveImageOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "SaveImageOperation.h"


@implementation SaveImageOperation

- (id)initWithImageOuput:(id<ImageOutput>)output {
	self = [super init];
	if(self) {
		imageOutput = output;
	}
	
	return self;
}

- (void)main {
}

@end
