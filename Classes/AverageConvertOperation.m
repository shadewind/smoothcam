//
//  AverageConvertOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-21.
//  Copyright 2010 N/A. All rights reserved.
//

#import "AverageConvertOperation.h"

#import "utils.h"
#import "YCbCrImage.h"
#import "GenericImage.h"
#import "imageprocessing.h"
#import "colorconversion.h"

@implementation AverageConvertOperation

- (void)setup:(Image *)firstImage {
	int width = firstImage.width;
	int height = firstImage.height;
	CGRect bounds = CGRectMake(0.0f, 0.0f, width, height);
	
	yContext = createGrayscaleBitmap(width, height);
	cbContext = createGrayscaleBitmap(width, height);
	crContext = createGrayscaleBitmap(width, height);
	
	CGContextSetGrayFillColor(cbContext, 0.5f, 1.0f);
	CGContextFillRect(cbContext, bounds);
	CGContextSetGrayFillColor(crContext, 0.5f, 1.0f);
	CGContextFillRect(crContext, bounds);
	
	metadata = firstImage.metadata;
	[metadata retain];
}

- (void)processImage:(Image *)image {
	YCbCrImage *ycbcrImage = (YCbCrImage *)image;
	if(![self isCancelled]) {
		CGContextSetAlpha(yContext, 1.0f / (self.currentImageIndex + 1));
		[ycbcrImage drawY:yContext];
	}
	
	if(![self isCancelled]) {
		CGContextSetAlpha(cbContext, 1.0f / (self.currentImageIndex + 1));
		[ycbcrImage drawCb:cbContext];
	}
	
	if(![self isCancelled]) {
		CGContextSetAlpha(crContext, 1.0f / (self.currentImageIndex + 1));
		[ycbcrImage drawCr:crContext];
	}
}

- (void)finish {
	int width = CGBitmapContextGetWidth(yContext);
	int height = CGBitmapContextGetHeight(yContext);
	unsigned char *yBase = CGBitmapContextGetData(yContext);
	int yStride = CGBitmapContextGetBytesPerRow(yContext);
	unsigned char *cbBase = CGBitmapContextGetData(cbContext);
	int cbStride = CGBitmapContextGetBytesPerRow(cbContext);
	unsigned char *crBase = CGBitmapContextGetData(crContext);
	int crStride = CGBitmapContextGetBytesPerRow(crContext);
	
	int rgbStride = width * 4;
	NSMutableData *rgbData = [NSMutableData dataWithLength:rgbStride * height];
	unsigned char *rgbBase = [rgbData mutableBytes];
	
	ycbrToRgb(width, height, yBase, yStride, cbBase, cbStride, crBase, crStride, rgbBase, rgbStride);
	
	CFRelease(yContext);
	yContext = NULL;
	CFRelease(cbContext);
	cbContext = NULL;
	CFRelease(crContext);
	crContext = NULL;
	
	CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)rgbData);
	CGImageRef rgbImage = CGImageCreate(width, height,
										8, 32, rgbStride, rgbColorspace(),
										kCGBitmapByteOrderDefault | kCGImageAlphaNoneSkipLast,
										dataProvider, NULL, true, kCGRenderingIntentDefault);
	CFRelease(dataProvider);
	GenericImage *image = [[GenericImage alloc] initWithCGImage:rgbImage id:@"combined"];
	CFRelease(rgbImage);
	image.metadata = metadata;
	[metadata release];
	metadata = nil;
	[self.output addImage:image withKey:self.key];
	[image release];
}

- (NSString *)progressString {
	if(self.currentImageIndex < self.totalImages)
		return @"Merging images...";
	else
		return @"Peforming color conversion...";
}

- (void)dealloc {
	if(yContext)
		CFRelease(yContext);
	if(cbContext)
		CFRelease(cbContext);
	if(crContext)
		CFRelease(crContext);
	[metadata release];
	[super dealloc];
}


@end
