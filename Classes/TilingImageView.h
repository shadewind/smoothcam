//
//  TilingImageView.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2011-01-15.
//  Copyright 2011 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TilingImageView : UIView {
}

@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIImage *lowresImage;

@end
