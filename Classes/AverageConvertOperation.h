//
//  AverageConvertOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-21.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MultiImageOperation.h"
#import "ImageMetadata.h"

@interface AverageConvertOperation : MultiImageOperation {
	CGContextRef yContext;
	CGContextRef cbContext;
	CGContextRef crContext;
	ImageMetadata *metadata;
}

@end
