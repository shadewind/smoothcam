//
//  ImageOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-17.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageOperation : NSOperation {
	float cost;
}

@property (nonatomic) float cost;

+ (id)operation;
- (NSString *)progressString;
- (float)completion;

@end
