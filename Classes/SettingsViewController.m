//
//  SettingsViewController.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-17.
//  Copyright 2010 N/A. All rights reserved.
//

#import "SettingsViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "settings.h"
#import "uiutils.h"

@interface SettingsViewController ()
- (void)layout;
@end

@implementation SettingsViewController

@synthesize flashOn, shootingMode, triggerMode, timerDelay, view, singleButton, multiButton, mergeButton,
            instantButton, timer3Button, timer10Button, flashButton, noflashButton, orientation;

- (id)init {
	self = [super init];
	if(self) {
		[[NSBundle mainBundle] loadNibNamed:@"SettingsView" owner:self options:nil];
		size = view.bounds.size;
		self.view.hidden = YES;
		CALayer *layer = self.view.layer;
		layer.cornerRadius = 20.0f;
		layer.borderColor = [[UIColor colorWithWhite:1.0f alpha:0.2f] CGColor];
		layer.borderWidth = 1.0f;
		
		[self resetDefaults];
	}
	
	return self;
}

- (IBAction)resetDefaults {
	self.shootingMode = kShootingModeSingle;
	self.triggerMode = kTriggerModeInstant;
	self.timerDelay = 3;
	self.flashOn = NO;
}

- (void)setFlashOn:(BOOL)on {
	flashOn = on;
	flashButton.selected = on;
	noflashButton.selected = !on;
}

- (void)setShootingMode:(ShootingMode)mode {
	shootingMode = mode;
	singleButton.selected = NO;
	multiButton.selected = NO;
	mergeButton.selected = NO;
	
	if(mode == kShootingModeSingle)
		singleButton.selected = YES;
	else if(mode == kShootingModeMulti)
		multiButton.selected = YES;
	else if(mode == kShootingModeMerge)
		mergeButton.selected = YES;
}

- (void)setTriggerMode:(TriggerMode)mode {
	triggerMode = mode;
	instantButton.selected = NO;
	timer3Button.selected = NO;
	timer10Button.selected = NO;
	
	if(mode == kTriggerModeInstant) {
		instantButton.selected = YES;
	} else if(mode == kTriggerModeTimer) {
		if(timerDelay == 3)
			timer3Button.selected = YES;
		if(timerDelay == 10)
			timer10Button.selected = YES;
	}
}

- (void)setTimerDelay:(int)delay {
	timerDelay = delay;
	
	if(triggerMode == kTriggerModeTimer)
		self.triggerMode = kTriggerModeTimer; //Select the correct button
}

- (void)shootingModeChanged:(id)sender {
	if(sender == singleButton)
		self.shootingMode = kShootingModeSingle;
	else if(sender == multiButton)
		self.shootingMode = kShootingModeMulti;
	else if(sender == mergeButton)
		self.shootingMode = kShootingModeMerge;
}

- (IBAction)triggerModeChanged:(id)sender {
	if(sender == instantButton) {
		self.triggerMode = kTriggerModeInstant;
	} else if(sender == timer3Button) {
		self.triggerMode = kTriggerModeTimer;
		self.timerDelay = 3;
	} else if(sender == timer10Button) {
		self.triggerMode = kTriggerModeTimer;
		self.timerDelay = 10;
	}
}

- (IBAction)flashOnChanged:(id)sender {
	if(sender == flashButton)
		self.flashOn = YES;
	else 
		self.flashOn = NO;
}

- (void)setOrientation:(UIInterfaceOrientation)o {
	orientation = o;
	[self layout];
}

- (void)setOrigin:(CGPoint)p {
	origin = p;
	[self layout];
}

- (CGPoint)origin {
	return origin;
}

- (void)setPadding:(CGFloat)pad {
	padding = pad;
	[self layout];
}

- (CGFloat)padding {
	return padding;
}

- (IBAction)toggle {
	if(visible)
		[self hide];
	else
		[self show];
	
}

- (IBAction)show {
	if(!visible) {
		CGRect frame = [view convertRect:view.bounds toView:view.superview];
		CGPoint to = view.center;
		view.center = CGPointMake(to.x, to.y + CGRectGetHeight(frame));
		view.hidden = NO;
		[UIView animateWithDuration:0.3f animations:^{
			view.center = to;
		}];
		visible = YES;
	}
}

- (IBAction)hide {
	if(visible) {
		CGRect frame = [view convertRect:view.bounds toView:view.superview];
		CGPoint from = view.center;
		CGPoint to = CGPointMake(from.x, from.y + CGRectGetHeight(frame));
		
		void (^animation)(void) = ^{
			view.center = to;
		};
		
		void (^completion)(BOOL) = ^(BOOL finished){
			view.hidden = YES;
			view.center = from;
		};
		
		[UIView animateWithDuration:0.3f animations:animation completion:completion];
		visible = NO;
	}
}

- (void)layout {
	float angle = interfaceOrientationToAngle(orientation);
	CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(angle);
	CGRect newBounds;
	
	if(orientation == UIInterfaceOrientationPortrait) {
		newBounds = CGRectMake(0.0f, 0.0f, size.width, size.height + padding);
	} else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
		newBounds = CGRectMake(0.0f, -padding, size.width, size.height + padding);
	} else if(orientation == UIInterfaceOrientationLandscapeRight) {
		newBounds = CGRectMake(0.0f, 0.0f, size.width + padding, size.height);
	} else if(orientation == UIInterfaceOrientationLandscapeLeft) {
		newBounds = CGRectMake(-padding, 0.0f, size.width + padding, size.height);
	}
	
	CGRect tBounds = CGRectApplyAffineTransform(newBounds, rotationTransform);
	CGPoint center = CGPointMake(origin.x + (CGRectGetWidth(tBounds) / 2), origin.y - (CGRectGetHeight(tBounds) / 2));
	[UIView animateWithDuration:0.3f animations:^{
		view.center = center;
		view.transform = rotationTransform;
		view.bounds = newBounds;
	}];
}

- (void)dealloc {
	self.view = nil;
	self.singleButton = nil;
	self.multiButton = nil;
	self.mergeButton = nil;
	self.instantButton = nil;
	self.timer3Button = nil;
	self.timer10Button = nil;
	self.flashButton = nil;
	self.noflashButton = nil;
	
    [super dealloc];
}


@end
