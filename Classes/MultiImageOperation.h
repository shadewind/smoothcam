//
//  MultiImageOperation.h
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-23.
//  Copyright 2010 N/A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageOperation.h"
#import "ImageProducer.h"
#import "ImageDestination.h"

@interface MultiImageOperation : ImageOperation <ImageProducer, ImageDestination> {
@private
	NSMutableArray *inputImages;
	int totalImages;
	int currentImage;
	
	id<ImageDestination> output;
	NSString *key;
}

@property (nonatomic, retain) id<ImageDestination> output;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, readonly) int currentImageIndex;
@property (nonatomic, readonly) int totalImages;

- (void)addImages:(NSArray *)images;

- (void)setup:(Image *)firstImage;
- (void)processImage:(Image *)image;
- (void)finish;

@end
