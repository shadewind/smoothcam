//
//  AlignImagesOperation.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-16.
//  Copyright 2010 N/A. All rights reserved.
//

#import "AlignImagesOperation.h"

#import "Image.h"
#import "GenericImage.h"
#import "imageprocessing.h"
#import "debug.h"

static const float kEdgeClearance = 100.0f;

@interface AlignImagesOperation ()
- (void)processReference:(Image *)image;
- (void)alignImage:(Image *)image;
@end

@implementation AlignImagesOperation

- (void)processImage:(Image *)image {
	if(grayscaleRef == nil)
		[self processReference:image];
	else
		[self alignImage:image];

	if(![self isCancelled]) {
		[self.output addImage:image withKey:self.key];
		[image unloadImage];
	}
}

- (void)processReference:(Image *)image {
	grayscaleRef = [image grayscaleImage];
	[grayscaleRef retain];
	
	area = CGRectInset(grayscaleRef.bounds, grayscaleRef.width * 0.05f, grayscaleRef.height * 0.05f);
	NSDictionary *points = findCornerPoints(grayscaleRef, area, 5);
	selectPoints(points, alignPoint);
}

- (void)alignImage:(Image *)image {
	CGPoint fromPoint[2];
	GenericImage *grayscaleImage = [image grayscaleImage];
	
	if(![self isCancelled]) {
		multiPassPositionAlign(grayscaleRef, grayscaleImage, area, alignPoint[0], 5);
		fromPoint[0] = CGPointApplyAffineTransform(alignPoint[0], CGAffineTransformInvert(grayscaleImage.imageTransform));
	}
	
	if(![self isCancelled]) {
		multiPassPositionAlign(grayscaleRef, grayscaleImage, area, alignPoint[1], 5);
		fromPoint[1] = CGPointApplyAffineTransform(alignPoint[1], CGAffineTransformInvert(grayscaleImage.imageTransform));
	}
	
	image.imageTransform = registrationTransform2p(fromPoint, alignPoint);
}

- (void)finish {
	[grayscaleRef release];
	grayscaleRef = nil;
}

- (NSString *)progressString {
	return @"Aligning images...";
}

- (void)dealloc {
	[grayscaleRef release];
	[super dealloc];
}

@end
