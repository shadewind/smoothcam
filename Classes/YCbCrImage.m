//
//  YCbCrImage.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-14.
//  Copyright 2010 N/A. All rights reserved.
//

#import "YCbCrImage.h"

#import <fcntl.h>

#import "GenericImage.h"
#import "imageprocessing.h"
#import "colorconversion.h"
#import "uiutils.h"

typedef struct {
	int plane;
	NSData *data;
} PlaneInfo;

size_t getPlaneBytes(void *info, void *buffer, off_t position, size_t count) {
	PlaneInfo *pinfo = (PlaneInfo *)info;
	const unsigned char *base = [pinfo->data bytes];
	for(int i = 0; i < count; i++) {
		unsigned char *dest = (unsigned char *)buffer + i;
		*dest = *(base + (position + i) * 2 + pinfo->plane);
	}
	
	return count;
}

void releasePlane(void *info) {
	PlaneInfo *pinfo = (PlaneInfo *)info;
	[pinfo->data release];
	free(pinfo);
}

CGDataProviderDirectCallbacks planeCallbacks = { 0, NULL, NULL, &getPlaneBytes, &releasePlane };

@interface YCbCrImage ()
- (CGImageRef)createCGImage;
- (CGImageRef)createYPlaneCGImage;
- (CGImageRef)createCbPlaneCGImage;
- (CGImageRef)createCrPlaneCGImage;
@end

@implementation YCbCrImage

@synthesize yPlane, yStride, cbcrPlane, cbcrStride, width, height;

- (id)initWithSampleBuffer:(CMSampleBufferRef)buffer id:(NSString *)imgId {
	self = [super initWithId:imgId];
	if(self) {
		CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(buffer);
		CVPixelBufferLockBaseAddress(pixelBuffer, 0);
		
		width = CVPixelBufferGetWidth(pixelBuffer);
		height = CVPixelBufferGetHeight(pixelBuffer);
		yStride = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
		yPlane = [[NSData alloc] initWithBytes:CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0)
										length:(yStride * height)];
		
		cbcrStride = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 1);
		cbcrPlane = [[NSData alloc] initWithBytes:CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1)
										 length:(cbcrStride * (height / 2))];
		
		CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
		
		[self.metadata addPropertiesFrom:(NSDictionary *)CMGetAttachment(buffer, CFSTR("MetadataDictionary"), NULL)];
	}
	
	return self;
}

- (GenericImage *)genericImage {
	[self saveTransform];
	self.imageTransform = CGAffineTransformIdentity;
	CGImageRef image = [self createCGImage];
	[self restoreTransform];
	
	GenericImage *genericImage = [[GenericImage alloc] initWithCGImage:image id:self.imageId];
	genericImage.imageTransform = self.imageTransform;
	genericImage.metadata = self.metadata;
	CFRelease(image);
	return [genericImage autorelease];
}

- (GenericImage *)grayscaleImage {
	CGImageRef image = [self createYPlaneCGImage];
	GenericImage *grayscale = [[GenericImage alloc] initWithCGImage:image id:self.imageId];
	CFRelease(image);
	return [grayscale autorelease];
}

- (CGImageRef)createYPlaneCGImage {
	if([self lock]) {
		CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)yPlane);
		CGImageRef image = CGImageCreate(width, height,
										 8, 8, yStride, grayColorspace(),
										 kCGBitmapByteOrderDefault | kCGImageAlphaNone,
										 provider, NULL, true, kCGRenderingIntentDefault);
		CFRelease(provider);
		[self unlock];
		return image;
	}
	
	return NULL;
}

- (CGImageRef)createCbPlaneCGImage {
	if([self lock]) {
		PlaneInfo *cbInfo = malloc(sizeof(PlaneInfo));
		cbInfo->plane = 0;
		cbInfo->data = cbcrPlane;
		[cbcrPlane retain];
		CGDataProviderRef cbProvider = CGDataProviderCreateDirect(cbInfo,
																  (width / 2) * (height / 2),
																  &planeCallbacks);
		CGImageRef cbImage = CGImageCreate(width / 2, height / 2,
										   8, 8, width / 2, grayColorspace(),
										   kCGBitmapByteOrderDefault | kCGImageAlphaNone,
										   cbProvider, NULL, true, kCGRenderingIntentDefault);
		CFRelease(cbProvider);
		return cbImage;
	}
	
	return NULL;
}

- (CGImageRef)createCrPlaneCGImage {
	if([self lock]) {
		PlaneInfo *crInfo = malloc(sizeof(PlaneInfo));
		crInfo->plane = 1;
		crInfo->data = cbcrPlane;
		[cbcrPlane retain];
		CGDataProviderRef crProvider = CGDataProviderCreateDirect(crInfo,
																  width / 2 * height / 2,
																  &planeCallbacks);
		CGImageRef crImage = CGImageCreate(width / 2, height / 2,
										   8, 8, width / 2, grayColorspace(),
										   kCGBitmapByteOrderDefault | kCGImageAlphaNone,
										   crProvider, NULL, true, kCGRenderingIntentDefault);
		CFRelease(crProvider);
		return crImage;
	}
	
	return NULL;
}

- (CGImageRef)createCGImage {
	if(![self lock])
		return NULL;
	
	CGContextRef yPlaneBitmap = CGBitmapContextCreate(NULL, width, height, 8, yStride,
												grayColorspace(), kCGBitmapByteOrderDefault | kCGImageAlphaNone);
	unsigned char *yBase = CGBitmapContextGetData(yPlaneBitmap);
	[self drawY:yPlaneBitmap];
	
	CGContextRef cbPlane = createGrayscaleBitmap(width, height);
	int cbStride = CGBitmapContextGetBytesPerRow(cbPlane);
	unsigned char *cbBase = CGBitmapContextGetData(cbPlane);
	[self drawCb:cbPlane];
	
	CGContextRef crPlane = createGrayscaleBitmap(width, height);
	int crStride = CGBitmapContextGetBytesPerRow(crPlane);
	unsigned char *crBase = CGBitmapContextGetData(crPlane);
	[self drawCr:crPlane];
	
	int rgbStride = width * 4;
	NSMutableData *rgbData = [NSMutableData dataWithLength:rgbStride * height];
	unsigned char *rgbBase = [rgbData mutableBytes];
	
	ycbrToRgb(width, height, yBase, yStride, cbBase, cbStride, crBase, crStride, rgbBase, rgbStride);
	
	CFRelease(yPlaneBitmap);
	CFRelease(cbPlane);
	CFRelease(crPlane);
	
	CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)rgbData);
	CGImageRef image = CGImageCreate(width, height,
									 8, 32, rgbStride, rgbColorspace(),
									 kCGBitmapByteOrderDefault | kCGImageAlphaNoneSkipLast,
									 dataProvider, NULL, true, kCGRenderingIntentDefault);
	CFRelease(dataProvider);
	[self unlock];
	return image;
	
}

- (void)drawY:(CGContextRef)context {
	CGImageRef image = [self createYPlaneCGImage];
	CGContextSaveGState(context);
	CGContextConcatCTM(context, self.imageTransform);
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), image);
	CGContextRestoreGState(context);
	CFRelease(image);
}

- (void)drawCb:(CGContextRef)context {
	CGImageRef image = [self createCbPlaneCGImage];
	CGContextSaveGState(context);
	CGContextConcatCTM(context, self.imageTransform);
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), image);
	CGContextRestoreGState(context);
	CFRelease(image);
}

- (void)drawCr:(CGContextRef)context {
	CGImageRef image = [self createCrPlaneCGImage];
	CGContextSaveGState(context);
	CGContextConcatCTM(context, self.imageTransform);
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), image);
	CGContextRestoreGState(context);
	CFRelease(image);
}

- (void)drawPreview:(CGContextRef)context {
	[self drawY:context];
}

- (BOOL)loadFromFile:(NSURL *)file {
	int fd = open([[file path] UTF8String], O_RDONLY);
	if(fd < 0)
		return NO;
	
	NSFileHandle *handle = [[NSFileHandle alloc] initWithFileDescriptor:fd closeOnDealloc:YES];
	yPlane = [handle readDataOfLength:(yStride * height)];
	[yPlane retain];
	cbcrPlane = [handle readDataOfLength:(cbcrStride * (height / 2))];
	[cbcrPlane retain];
	[handle release];
	return YES;
}

- (NSURL *)unloadToFile {
	NSString *file = [NSTemporaryDirectory() stringByAppendingPathComponent:
					  [NSString stringWithFormat:@"ycbcr_%@.dat", self.imageId]];
	
	int fd = open([file UTF8String],
				  O_WRONLY | O_TRUNC | O_CREAT,
				  S_IRUSR | S_IWUSR | S_IRGRP);
	if(fd >= 0) {
		NSFileHandle *handle = [[NSFileHandle alloc] initWithFileDescriptor:fd closeOnDealloc:YES];
		[handle writeData:yPlane];
		[handle writeData:cbcrPlane];
		[handle release];
		
		[yPlane release];
		yPlane = nil;
		[cbcrPlane release];
		cbcrPlane = nil;
		return [NSURL fileURLWithPath:file];
	}
	else {
		return nil;
	}
}

- (void)dealloc {
	[yPlane release];
	[cbcrPlane release];
	
	[super dealloc];
}

@end
