    //
//  SmoothCamViewController.m
//  SmoothCam
//
//  Created by Emil Eriksson on 2010-12-13.
//  Copyright 2010 N/A. All rights reserved.
//

#import "SmoothCamViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "Image.h"
#import "CameraSession.h"
#import "JobQueue.h"
#import "Job.h"
#import "YCbCrImage.h"
#import "SettingsViewController.h"
#import "CameraView.h"
#import "AlignImagesOperation.h"
#import "ConvertImagesOperation.h"
#import "AverageConvertOperation.h"
#import "AverageImagesOperation.h"
#import "SaveToLibraryOperation.h"
#import "GalleryViewController.h"
#import "TextLayer.h"
#import "uiutils.h"
#import "utils.h"
#import "settings.h"

static const float pointOpacity = 0.7f;

@interface SmoothCamViewController ()
- (void)releaseView;
- (CGPoint)viewPointToVideoPoint:(CGPoint)p;
- (void)singleTap:(UIGestureRecognizer *)recognizer;
- (void)doubleTap:(UIGestureRecognizer *)recognizer;
- (void)setFocusPoint:(CGPoint)p;
- (void)setExposurePoint:(CGPoint)p;
- (void)showClearButton;
- (void)clearFocusExposure;
- (void)flashToNumber:(int)n;
- (void)timerFired;
- (void)changeOrientation;
- (void)addConvertJobs:(NSArray *)images;
@end

@implementation SmoothCamViewController

@synthesize cameraView, shutterButton, rotateViews;

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(self) {
		cameraSession = [[CameraSession alloc] init];
		cameraSession.delegate = self;
		jobQueue = [[JobQueue alloc] init];
		galleryViewController = [[GalleryViewController alloc] initWithNibName:@"GalleryView" bundle:nil];
		galleryViewController.jobQueue = jobQueue;
	}
	
	return self;
}

- (void)viewDidLoad {
	UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
	doubleTap.numberOfTapsRequired = 2;
	[cameraView addGestureRecognizer:doubleTap];
	[doubleTap release];
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
	singleTap.numberOfTapsRequired = 1;
	[singleTap requireGestureRecognizerToFail:doubleTap];
	[cameraView addGestureRecognizer:singleTap];
	[singleTap release];
	
	AVCaptureVideoPreviewLayer *previewLayer = cameraSession.previewLayer;
	previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
	previewLayer.frame = cameraView.layer.bounds;
	[cameraView.layer addSublayer:previewLayer];
	
	if(cameraSession.focusPointSupported) {
		focusPointLayer = [CALayer layer];
		focusPointLayer.contentsScale = 2.0f;
		focusPointLayer.bounds = CGRectMake(0.0f, 0.0f, 70.0f, 70.0f);
		focusPointLayer.delegate = self;
		focusPointLayer.opacity = 0.5f;
		[cameraView.layer addSublayer:focusPointLayer];
		[focusPointLayer setNeedsDisplay];
	}
	
	if(cameraSession.exposurePointSupported) {
		exposurePointLayer = [CALayer layer];
		exposurePointLayer.contentsScale = 2.0f;
		exposurePointLayer.bounds = CGRectMake(0.0f, 0.0f, 70.0f, 70.0f);
		exposurePointLayer.delegate = self;
		exposurePointLayer.opacity = 0.5;
		[cameraView.layer addSublayer:exposurePointLayer];
		[exposurePointLayer setNeedsDisplay];
	}
	
	clearButtonLayer = [CALayer layer];
	clearButtonLayer.contentsScale = 2.0f;
	clearButtonLayer.opacity = 0.7f;
	clearButtonLayer.anchorPoint = CGPointMake(1.0f, 0.0f);
	clearButtonLayer.bounds	= CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
	clearButtonLayer.position = CGPointMake(cameraView.bounds.size.width, 0.0f);
	clearButtonLayer.hidden = YES;
	clearButtonLayer.delegate = self;
	[cameraView.layer addSublayer:clearButtonLayer];
	[clearButtonLayer setNeedsDisplay];
	
	CGPoint center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
	
	pictureNumberLayer = [TextLayer layer];
	pictureNumberLayer.contentsScale = 2.0f;
	pictureNumberLayer.font = [UIFont fontWithName:@"Helvetica" size:self.view.bounds.size.width / 2.0f];
	pictureNumberLayer.color = [UIColor whiteColor];
	pictureNumberLayer.bounds = CGRectMake(0.0f, 0.0f, 160.0f, 160.0f);
	pictureNumberLayer.position = center;
	pictureNumberLayer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.1f] CGColor];
	pictureNumberLayer.cornerRadius = 10.0f;
	pictureNumberLayer.opacity = 0.0f;
	[cameraView.layer addSublayer:pictureNumberLayer];
	
	timerLayer = [CALayer layer];
	timerLayer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.4f] CGColor];
	timerLayer.bounds = CGRectMake(0.0f, 0.0f, 200.0f, 200.0f);
	timerLayer.position = center;
	timerLayer.cornerRadius = 10.0f;
	timerLayer.contents = (id)[[UIImage imageNamed:@"timer"] CGImage];
	timerLayer.opacity = 0.7f;
	timerLayer.hidden = YES;
	[cameraView.layer addSublayer:timerLayer];
	
	countdownLayer = [TextLayer layer];
	countdownLayer.contentsScale = 2.0f;
	countdownLayer.color = [UIColor whiteColor];
	countdownLayer.font = [UIFont fontWithName:@"Helvetica-Bold" size:100.0f];
	countdownLayer.frame = timerLayer.bounds;
	countdownLayer.position = CGPointMake(countdownLayer.position.x, countdownLayer.position.y + 12.0f);
	[timerLayer addSublayer:countdownLayer];
	
	flashLayer = [CALayer layer];
	flashLayer.frame = cameraView.layer.bounds;
	flashLayer.backgroundColor = [[UIColor whiteColor] CGColor];
	flashLayer.opacity = 0.0f;
	[cameraView.layer addSublayer:flashLayer];
	
	settingsViewController = [[SettingsViewController alloc] init];
	settingsViewController.origin = CGPointMake(5.0f, 480.0f);
	settingsViewController.padding = 54.0f;
	[settingsViewController addObserver:self forKeyPath:@"flashOn" options:NSKeyValueObservingOptionNew context:nil];
	[self.view insertSubview:settingsViewController.view aboveSubview:cameraView];
	
	[self clearFocusExposure];
}

- (void)viewWillAppear:(BOOL)animated {
	UIDevice *device = [UIDevice currentDevice];
	[device beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(changeOrientation)
												 name:UIDeviceOrientationDidChangeNotification
											   object:device];
}

- (IBAction)trigger {
	shutterButton.enabled = NO;
	
	if(settingsViewController.triggerMode == kTriggerModeTimer) {
		secondsLeft = settingsViewController.timerDelay;
		timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
												 target:self
											   selector:@selector(timerFired)
											   userInfo:nil
												repeats:YES];
		countdownLayer.text = nil;
		timerLayer.hidden = NO;
		[timer fire];
	} else {
		[self shoot];
	}

}

- (void)timerFired {
	if(secondsLeft == 0) {
		timerLayer.hidden = YES;
		[timer invalidate];
		[self shoot];
	} else {
		countdownLayer.text = [[NSNumber numberWithInt:secondsLeft] stringValue];
		secondsLeft--;
	}
}

- (IBAction)shoot {
	numPictures = 1;
	if(settingsViewController.shootingMode == kShootingModeMerge)
		numPictures = [[NSUserDefaults standardUserDefaults] integerForKey:kNumMergePhotosKey];
	else if(settingsViewController.shootingMode == kShootingModeMulti)
		numPictures = 3;
	
	[cameraSession shootPictures:numPictures];
	
	if(numPictures > 1)
		[self flashToNumber:numPictures - 1];
	else
		[self flashToNumber:-1];
}

- (IBAction)openGallery {
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:galleryViewController];
	navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	[self presentModalViewController:navController animated:YES];
	[navController release];
}

- (IBAction)openSettings {
	[settingsViewController toggle];
}

- (void)shootCompleteWithImages:(NSArray *)images error:(NSError *)error {
	if((settingsViewController.shootingMode == kShootingModeMerge) && ([images count] > 1)) {
		Job *job = [Job job];
		job.thumbnail = [[images objectAtIndex:0] previewThumbnail];
		AlignImagesOperation *alignOperation = [AlignImagesOperation operation];
		[alignOperation addImages:images];
		[job chainOperation:alignOperation];
		[job chainOperation:[AverageConvertOperation operation]];
		if([[NSUserDefaults standardUserDefaults] integerForKey:kOutputFileFormatKey] == kOutputFileFormatTIFF)
			job.outputFormat = (NSString *)kUTTypeTIFF;
		[jobQueue addJob:job];
		
		if([[NSUserDefaults standardUserDefaults] boolForKey:kSaveMergeReference])
			[self addConvertJobs:[NSArray arrayWithObject:[images objectAtIndex:0]]];
	} else {
		[self addConvertJobs:images];
	}
	
	shutterButton.enabled = YES;
}

- (void)addConvertJobs:(NSArray *)images {
	for(Image *image in images) {
		Job *job = [Job job];
		job.thumbnail = [image previewThumbnail];
		ConvertImagesOperation *convertOperation = [ConvertImagesOperation operation];
		[convertOperation addImage:image withKey:nil];
		[job chainOperation:convertOperation];
		
		if([[NSUserDefaults standardUserDefaults] integerForKey:kOutputFileFormatKey] == kOutputFileFormatTIFF)
			job.outputFormat = (NSString *)kUTTypeTIFF;
		
		[jobQueue addJob:job];
	}
}

- (void)shotPicture:(YCbCrImage *)picture withIndex:(int)i {
	if((numPictures - i) > 1)
		[self flashToNumber:numPictures - i - 2];
	
	NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
		[picture unloadImage];
	}];
	[jobQueue addSingleOperation:operation];
}

- (void)flashToNumber:(int)n {
	pictureNumberLayer.text = [[NSNumber numberWithInt:n] stringValue];
	[CATransaction begin];
	[CATransaction setDisableActions:YES];
	[CATransaction commit];
	
	[CATransaction begin];
	if(n >= 0) {
		CABasicAnimation *fadeNumberAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
		fadeNumberAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
		fadeNumberAnimation.duration = 2.0f;
		[pictureNumberLayer addAnimation:fadeNumberAnimation forKey:nil];
	}
	
	CABasicAnimation *flashAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	flashAnimation.fromValue = [NSNumber numberWithFloat:1.0];
	flashAnimation.duration = 0.3f;
	[flashLayer addAnimation:flashAnimation forKey:nil];
	[CATransaction commit];
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
	UIGraphicsPushContext(ctx);
	
	if(layer == focusPointLayer) {
		CGMutablePathRef path = CGPathCreateMutable();
		CGPathMoveToPoint(path, NULL, 5.0f, 25.0f);
		CGPathAddLineToPoint(path, NULL, 5.0f, 5.0f);
		CGPathAddLineToPoint(path, NULL, 25.0f, 5.0f);
		
		CGPathMoveToPoint(path, NULL, 45.0f, 5.0f);
		CGPathAddLineToPoint(path, NULL, 65.0f, 5.0f);
		CGPathAddLineToPoint(path, NULL, 65.0f, 25.0f);
		
		CGPathMoveToPoint(path, NULL, 65.0f, 45.0f);
		CGPathAddLineToPoint(path, NULL, 65.0f, 65.0f);
		CGPathAddLineToPoint(path, NULL, 45.0f, 65.0f);
		
		CGPathMoveToPoint(path, NULL, 25.0f, 65.0f);
		CGPathAddLineToPoint(path, NULL, 5.0f, 65.0f);
		CGPathAddLineToPoint(path, NULL, 5.0f, 45.0f);
		
		CGContextSetLineCap(ctx, kCGLineCapRound);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor blackColor] CGColor]);
		CGContextSetLineWidth(ctx, 4.0f);
		CGContextAddPath(ctx, path);
		CGContextStrokePath(ctx);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
		CGContextSetLineWidth(ctx, 2.0f);
		CGContextAddPath(ctx, path);
		CGContextStrokePath(ctx);
		
		CFRelease(path);
	} else if(layer == exposurePointLayer) {
		CGRect ellipse = CGRectMake(15.0f, 15.0f, 40.0f, 40.0f);
		
		CGContextSetStrokeColorWithColor(ctx, [[UIColor blackColor] CGColor]);
		CGContextSetLineWidth(ctx, 4.0f);
		CGContextStrokeEllipseInRect(ctx, ellipse);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
		CGContextSetLineWidth(ctx, 2.0f);
		CGContextStrokeEllipseInRect(ctx, ellipse);
		
		CGMutablePathRef path = CGPathCreateMutable();
		CGPathMoveToPoint(path, NULL, 25.0f, 35.0f);
		CGPathAddLineToPoint(path, NULL, 45.0f, 35.0f);
		CGPathMoveToPoint(path, NULL, 35.0f, 25.0f);
		CGPathAddLineToPoint(path, NULL, 35.0f, 45.0f);
		
		CGContextSetLineCap(ctx, kCGLineCapRound);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor blackColor] CGColor]);
		CGContextSetLineWidth(ctx, 4.0f);
		CGContextAddPath(ctx, path);
		CGContextStrokePath(ctx);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
		CGContextSetLineWidth(ctx, 2.0f);
		CGContextStrokeEllipseInRect(ctx, ellipse);
		CGContextAddPath(ctx, path);
		CGContextStrokePath(ctx);
		
		CFRelease(path);
	} else if(layer == clearButtonLayer) {
		CGRect ellipse = CGRectMake(3.0f, -47.0f, 94.0f, 94.0f);
		CGContextSetFillColorWithColor(ctx, [[UIColor colorWithWhite:0.0f alpha:0.7f] CGColor]);
		CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
		CGContextSetLineWidth(ctx, 3.0f);
		CGContextFillEllipseInRect(ctx, ellipse);
		CGContextStrokeEllipseInRect(ctx, ellipse);
		
		CGContextTranslateCTM(ctx, 25.0f, 10.0f);
		CGContextMoveToPoint(ctx, 0.0f, 0.0f);
		CGContextAddLineToPoint(ctx, 15.0f, 15.0f);
		CGContextMoveToPoint(ctx, 15.0f, 0.0f);
		CGContextAddLineToPoint(ctx, 0.0f, 15.0f);
		
		CGContextSetLineWidth(ctx, 4.0f);
		CGContextStrokePath(ctx);
	}
	
	UIGraphicsPopContext();
}

- (void)singleTap:(UIGestureRecognizer *)recognizer {
	CGPoint p = [recognizer locationInView:cameraView];
	CGPoint cp = clearButtonLayer.position;
	
	if((hypot(p.x - cp.x, p.y - cp.y) < 50.0f) && !clearButtonLayer.hidden) {
		[self clearFocusExposure];
	} else {
		[self setFocusPoint:p];
		SingleTapAction action = [[NSUserDefaults standardUserDefaults] integerForKey:kSingleTapActionKey];
		if(action == kSingleTapActionFocusExposure)
			[self setExposurePoint:p];
	}
}

- (void)doubleTap:(UIGestureRecognizer *)recognizer {
	CGPoint p = [recognizer locationInView:cameraView];
	[self setExposurePoint:p];
}

- (void)clearFocusExposure {
	CGPoint center = CGPointMake(CGRectGetMidX(cameraView.bounds),
								 CGRectGetMidY(cameraView.bounds));
	[self setExposurePoint:center];
	
	
	[cameraSession setFocusPoint:CGPointMake(0.5f, 0.5f)
							mode:AVCaptureFocusModeContinuousAutoFocus];
	focusPointLayer.position = center;
	
	if(!clearButtonLayer.hidden) {
		[CATransaction begin];
		[CATransaction setAnimationDuration:0.2f];
		clearButtonLayer.hidden = YES;
		[CATransaction commit];
	}
}

- (void)setFocusPoint:(CGPoint)p {
	if(cameraSession.focusPointSupported) {
		CGPoint videoPoint = [self viewPointToVideoPoint:p];
		TapFocusMode mode = [[NSUserDefaults standardUserDefaults] integerForKey:kTapFocusModeKey];
		if(mode == kTapFocusModeLocked)
			[cameraSession setFocusPoint:videoPoint mode:AVCaptureFocusModeAutoFocus];
		else
			[cameraSession setFocusPoint:videoPoint mode:AVCaptureFocusModeContinuousAutoFocus];
		[self showClearButton];
		
		[CATransaction begin];
		[CATransaction setDisableActions:YES];
		focusPointLayer.position = p;
		[CATransaction commit];
		
		[CATransaction begin];
		[CATransaction setAnimationDuration:0.2f];
		
		CABasicAnimation *drop = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
		drop.duration = 0.2f;
		drop.fromValue = [NSNumber numberWithFloat:20.0f];
		drop.toValue = [NSNumber numberWithFloat:1.0f];
		[focusPointLayer addAnimation:drop forKey:@"drop"];
		
		[CATransaction commit];
	}
}

- (void)setExposurePoint:(CGPoint)p {
	if(cameraSession.exposurePointSupported) {
		CGPoint videoPoint = [self viewPointToVideoPoint:p];
		[cameraSession setExposurePoint:videoPoint mode:AVCaptureExposureModeContinuousAutoExposure];
		[self showClearButton];
		
		[CATransaction begin];
		[CATransaction setAnimationDuration:0.2f];
		exposurePointLayer.position = p;
		[CATransaction commit];
	}
}

- (void)showClearButton {
	if(clearButtonLayer.hidden) {
		[CATransaction begin];
		[CATransaction setDisableActions:YES];
		clearButtonLayer.hidden = NO;
		CABasicAnimation *slide = [CABasicAnimation animationWithKeyPath:@"position"];
		slide.duration = 0.1f;
		float width = cameraView.bounds.size.width;
		slide.fromValue = [NSValue valueWithCGPoint:CGPointMake(width + clearButtonLayer.frame.size.width, 0.0f)];
		[clearButtonLayer addAnimation:slide forKey:@"slide"];
		[CATransaction commit];
	}
}

- (void)focusStatusChanged:(BOOL)isFocusing {
	if(isFocusing) {
		CABasicAnimation *blink = [CABasicAnimation animationWithKeyPath:@"opacity"];
		blink.duration = 0.1f;
		blink.fromValue = [NSNumber numberWithFloat:1.0f];
		blink.toValue = [NSNumber numberWithFloat:0.0f];
		blink.autoreverses = YES;
		blink.repeatCount = HUGE_VALF;
		blink.beginTime = CACurrentMediaTime() + 0.3f;
		[focusPointLayer addAnimation:blink forKey:@"blink"];
	} else {
		[focusPointLayer removeAllAnimations];
	}
}

- (CGPoint)viewPointToVideoPoint:(CGPoint)p {
	CGRect videoFrame = cameraSession.videoPreviewFrame;
	CGPoint tp = CGPointMake((p.y - videoFrame.origin.y) / videoFrame.size.height,
							 1.0f - ((p.x - videoFrame.origin.x) / videoFrame.size.width));
	tp.x = clampf(0.0f, tp.x, 1.0f);
	tp.y = clampf(0.0f, tp.y, 1.0f);
	return tp;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	[cameraSession setFlash:settingsViewController.flashOn];
}

- (void)changeOrientation {
	UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
	if(UIDeviceOrientationIsValidInterfaceOrientation(orientation)) {
		cameraSession.orientation = orientation;
		settingsViewController.orientation = orientation;
		
		CGAffineTransform transform = CGAffineTransformMakeRotation(interfaceOrientationToAngle(orientation));
		[UIView animateWithDuration:0.3f animations:^{
			for(UIView *view in rotateViews)
				view.transform = transform;
		}];
		
		[pictureNumberLayer setAffineTransform:transform];
		[timerLayer setAffineTransform:transform];
	}
}

- (void)viewWillDisappear:(BOOL)animated {
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)releaseView {
	[settingsViewController release];
	settingsViewController = nil;
	self.cameraView = nil;
	self.shutterButton = nil;
	self.rotateViews = nil;
}

- (void)viewDidUnload {
	[self releaseView];
}

- (void)dealloc {
	[self releaseView];
	[cameraSession release];
	[jobQueue release];
	[galleryViewController release];
	
    [super dealloc];
}


@end
